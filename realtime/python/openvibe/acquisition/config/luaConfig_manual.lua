-- this function is called when the box is initialized
function initialize(box)

	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
end

-- this function is called when the box is uninitialized
function uninitialize(box)
	io.write("uninitialize has been called\n")
end


function process(box)
	lr = -1
	stimulusCount = 0
	timeBetweenStimulus = 25
	timeOffset = 1*timeBetweenStimulus
	numberOfChunks = 6
	trialLength = 60
	-- loop until box:keep_processing() returns zero
	-- cpu will be released with a call to sleep
	-- at the end of the loop
	while box:keep_processing() do
	
		-- gets current simulated time
		t = box:get_current_time()
		

		input_stim = ""
		input_stim = box:get_stimulation(1, 1)
		box:remove_stimulation(1, box:get_stimulation_count(1))

		if input_stim == OVTK_StimulationId_Label_0A then
		
		box:send_stimulation(3, OVTK_GDF_End_Of_Session, box:get_current_time())
	    box:sleep()

		end
		
		
		if input_stim == OVTK_StimulationId_Label_01 then
			
			
			
			t_start = box:get_current_time()

			box:send_stimulation(1, OVTK_GDF_Left, box:get_current_time())
			box:send_stimulation(2, OVTK_GDF_Left, box:get_current_time())
			box:send_stimulation(1, OVTK_StimulationId_VisualStimulationStop, box:get_current_time()+3)
			
			while box:get_current_time() < t_start+trialLength do
			box:sleep()
			end
			box:send_stimulation(2, OVTK_GDF_End_Of_Trial, box:get_current_time())
			box:send_stimulation(1, OVTK_StimulationId_TrialStop, box:get_current_time())
			
			t_start = box:get_current_time()
			while box:get_current_time() < t_start+0.5 do
			box:sleep()
			end
		end
		
		
		if input_stim == OVTK_StimulationId_Label_02 then
			
			
			
			t_start = box:get_current_time()

			box:send_stimulation(1, OVTK_GDF_Right, box:get_current_time())
			box:send_stimulation(2, OVTK_GDF_Right, box:get_current_time())
			box:send_stimulation(1, OVTK_StimulationId_VisualStimulationStop, box:get_current_time()+3)
			
			while box:get_current_time() < t_start+trialLength do
			box:sleep()
			end

			box:send_stimulation(2, OVTK_GDF_End_Of_Trial, box:get_current_time())
			box:send_stimulation(1, OVTK_StimulationId_TrialStop, box:get_current_time())
			
			t_start = box:get_current_time()
			while box:get_current_time() < t_start+0.5 do
			box:sleep()
			end
		end
				
		
		--box:remove_stimulation(1, box:get_stimulation_count(1))

		-- releases cpu
		box:sleep()
	end
	
end
