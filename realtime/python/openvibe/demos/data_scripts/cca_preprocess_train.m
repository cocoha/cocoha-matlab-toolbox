% Example script to preprocess training data using the
% COCOHA-matlab-toolbox. Initialize the toolbox and fieldtrip before
% running. The file stimulations.txt contain indexes for attending
% left/right at trial onset and end-of-trial

clear
cd(fileparts(matlab.desktop.editor.getActiveFilename))

%% Load dataset
cfg = [];
cfg.eeg.dataset = fullfile('..','data','traindata.gdf');
data = co_preprocessing(cfg,[]);

%% Read in stimulation data indicating attend left/right
filename = fullfile('..','data','stimulations_train.txt');

delimiter = ','; formatSpec = '%f%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
fclose(fileID);

dataArray(1) = cellfun(@(x) num2cell(x), dataArray(1), 'UniformOutput', false);
stimulations = [dataArray{1:end-1}];
clearvars filename delimiter formatSpec fileID dataArray;

%% Add stimulations to data
data.event.eeg.sample = round(cell2mat(stimulations(:,1))*data.fsample.eeg);
data.event.eeg.value = stimulations(:,2);
%clear stimulations

%% ID stimulation indexes
right_ix = find(strcmp(data.event.eeg.value,'OVTK_GDF_Right'));
left_ix = find(strcmp(data.event.eeg.value,'OVTK_GDF_Left'));
end_ix = find(strcmp(data.event.eeg.value,'OVTK_GDF_End_Of_Trial'));

%% Trim 5s off beginning and end of trials
data.event.eeg.sample(right_ix) = data.event.eeg.sample(right_ix) + data.fsample.eeg*5;
data.event.eeg.sample(left_ix) = data.event.eeg.sample(left_ix) + data.fsample.eeg*5;
data.event.eeg.sample(end_ix) = data.event.eeg.sample(end_ix) - data.fsample.eeg*5;

%% Separate EEG and audio
cfg = []; cfg.eeg.dim = 'chan';
cfg.eeg.select = {'FrontLeft','FrontRight'};
data_wav = co_selectdim(cfg,data);
cfg.eeg.select = {'all','-FrontLeft','-FrontRight'};
data = co_selectdim(cfg,data);

%% Initial filtering
cfg = [];
% cfg.eeg.demean = 'yes';
cfg.eeg.detrend = 1;    % Minimize startup artifacts
cfg.eeg.hpfilter = 'yes';
cfg.eeg.hpfilttype = 'butter'; %'firws';
cfg.eeg.hpfiltord = 2; %data.fsample.eeg*50;
cfg.eeg.hpfiltdir = 'onepass';
cfg.eeg.hpfreq = 0.1;
data = co_preprocessing(cfg,data);

%% Average reference
cfg = [];
cfg.eeg.channels = {'all'};
cfg.eeg.reref = 'yes';
cfg.eeg.refchannel = 'all';
data = co_preprocessing(cfg,data);


%% Denoising
cfg = [];
cfg.eeg.layout = 'biosemi64.lay';
cfg.eeg.eog.channels = {'Channel 1','Channel 34'};
cfg.eeg.eog.samples = {data.event.eeg.sample(min([left_ix;right_ix])):size(data.eeg{1},1)};
cfg.eeg.eog.realtime = [];
cfg.eeg.eog.dssthresh = -1;
data = co_denoise(cfg,data);

% Save denoising matrices
realtime = data.cfg{end}.eeg.eog.realtime{1};
save('eog.mat','-struct','realtime'); clear realtime;

%% Average reference
cfg = [];
cfg.eeg.reref = 'yes';
cfg.eeg.refchannel = 'all';
data = co_preprocessing(cfg,data);

%% Split data into trials and assign appropriate audio
cfg = [];
cfg.eeg.splitsample = data.event.eeg.sample(union(left_ix,right_ix));
data = co_splitdata(cfg,data);
data_wav = co_splitdata(cfg,data_wav);

for ii = 2:length(data.eeg)
    if strcmp(data.event.eeg(ii).value{1},'OVTK_GDF_Left')  % Subject attending to audio channel 2 (physical channels were reversed)
        data_wav.eeg{ii} = fliplr(data_wav.eeg{ii});        % Swap channel 2 into 'attended position'
    end
end

data.wavA = data_wav.eeg;
data.fsample.wavA = data_wav.fsample.eeg;
data.dim.wavA = data_wav.dim.eeg;
data.event.wavA = data_wav.event.eeg;

cfg = [];
cfg.wavA.dim = 'chan';
cfg.wavA.select = 2;
dataB = co_selectdim(cfg,data);
cfg.wavA.select = 1;
data = co_selectdim(cfg,data);
data.wavB = dataB.wavA;
data.fsample.wavB = dataB.fsample.wavA;
data.dim.wavB = dataB.dim.wavA;
data.event.wavB = dataB.event.wavA;

%% Remove noise samples
emptycell = [];
for ii = 1:length(data.eeg)
    eotix = strcmp(data.event.eeg(ii).value,'OVTK_GDF_End_Of_Trial');
    if all(eotix==0); emptycell = cat(1,emptycell,ii); end
    
    cfg = [];
    cfg.eeg.cell = ii; cfg.eeg.dim = 'time'; cfg.eeg.select = 1:data.event.eeg(ii).sample(eotix);
    cfg.wavA.cell = ii; cfg.wavA.dim = 'time'; cfg.wavA.select = 1:data.event.eeg(ii).sample(eotix);
    cfg.wavB.cell = ii; cfg.wavB.dim = 'time'; cfg.wavB.select = 1:data.event.eeg(ii).sample(eotix);
    data = co_selectdim(cfg,data);
end

%% Remove trials with no OVTK_GDF_End_Of_Trial markers (empty)
if ~isempty(emptycell)
    cfg = [];
    cfg.eeg.cell = setdiff(1:length(data.eeg),emptycell);
    cfg.wavA.cell = setdiff(1:length(data.wavA),emptycell);
    cfg.wavB.cell = setdiff(1:length(data.wavB),emptycell);
    data = co_selectcell(cfg,data);
end

%%
save('data_train.mat','data','-v7.3');