clear;
MATBASEPATH = '.';
KERNELLEN = 2;
TRIALLEN = [3,5,7,10,15,20,30] - KERNELLEN;
TRIALSTEP = 1;

load('data_debug.mat')

%% Compress audio envelope
for ii = 1:length(data.wavA)
    data.wavA{ii} = max(data.wavA{ii},0).^0.3;
    data.wavB{ii} = max(data.wavB{ii},0).^0.3;
end

%% Use multismooth to filterbank EEG data
cfg = [];
cfg.eeg.kernelsizes = round(2.^unique(0:0.5:7));
data = co_multismooth(cfg,data);

cfg = [];
cfg.eeg.dim = 'smooth';
data = co_diff(cfg,data);

%% Use multismooth for audio filterbank
cfg = [];
cfg.wavA.kernelsizes = round(2.^unique(0:0.5:7));
cfg.wavB.kernelsizes = round(2.^unique(0:0.5:7));
data = co_multismooth(cfg,data);

cfg = [];
cfg.wavA.dim = 'smooth';
cfg.wavB.dim = 'smooth';
data = co_diff(cfg,data);

%% Unsplit data
cfg = [];
cfg.eeg.dim = 'time'; cfg.wavA.dim = 'time'; cfg.wavB.dim = 'time';
data = co_unsplitdata(cfg,data);

%% Whiten data
load('whiten_eeg_smooth.mat');
load('whiten_eeg_chan.mat');
load('pca_wav.mat');

cfg = [];
cfg.eeg.W = W_s;
cfg.eeg.operation = 'whiten';
cfg.eeg.whitedim = {'smooth'};
cfg.eeg.compix = 1:bestComp(1);
data_wt = co_whitendata(cfg,data);

cfg = [];
cfg.eeg.W = W_f;
cfg.eeg.operation = 'whiten';
cfg.eeg.whitedim = {'chan'};
cfg.eeg.compix = 1:bestComp(2);
data_wt = co_whitendata(cfg,data_wt);

%% PCA Audio ??
cfg = [];
cfg.wavA.W{1} = components.wavA.W; cfg.wavB.W{1} = components.wavA.W;
cfg.wavA.components{1} = 1:bestComp(1); cfg.wavB.components{1} = 1:bestComp(1);
data_wt = co_dimreduction(cfg,data_wt);

%% Perform CCA on data
cca_decoder = CoDecoder('cca_decoder.mat');

% Attended
cfg = [];
cfg.input.field = 'eeg';
cfg.output.field = 'wavA';
data_ccaA = co_decode_regression(cfg,data_wt,cca_decoder);

% Unattended
cfg = [];
cfg.input.field = 'eeg';
cfg.output.field = 'wavB';
data_ccaB = co_decode_regression(cfg,data_wt,cca_decoder);

%% Evaluate performance
performance = cell(1,length(TRIALLEN));
fit = []; fit.dprime = cell(1,length(TRIALLEN));
for tt = 1:length(TRIALLEN)
    fprintf('Time window: %ds\n',TRIALLEN(tt));
    %% Compute attended features (Attended decoder, attended audio)
    cr_a = [];
    for ii = 1:TRIALSTEP*data.fsample.eeg:size(data_ccaA.eeg{1},1)-TRIALLEN(tt)*data.fsample.eeg;
        ix = ii:ii+TRIALLEN(tt)*data.fsample.eeg-1;
        r = nt_normcol(data_ccaA.eeg{1}(ix,:))' * nt_normcol(data_ccaA.wavA{1}(ix,:)) ./ length(ix);
        if isempty(cr_a)
            cr_a = [diag(r)]';
        else
             cr_a = [cr_a; diag(r)'];
        end
    end

    %% Compute unattended features (Attended decoder, unattended audio)
    cr_u = [];
    for ii = 1:TRIALSTEP*data.fsample.eeg:size(data_ccaB.eeg{1},1)-TRIALLEN(tt)*data.fsample.eeg
        ix = ii:ii+TRIALLEN(tt)*data.fsample.eeg-1;
        r = nt_normcol(data_ccaB.eeg{1}(ix,:))' * nt_normcol(data_ccaB.wavB{1}(ix,:)) ./ length(ix);
        if isempty(cr_u)
            cr_u = [diag(r)]';
        else
            cr_u = [cr_u; diag(r)'];
        end
    end
    
    %% Compute d-prime (for current triallen)
    fit.dprime{tt} = (mean(cr_a,1) - mean(cr_u,1))./sqrt(0.5*(var(cr_a,[],1)+var(cr_u,[],1)));

    %% Create combined detection dataset
    noflipix = 1:2:size(cr_a,1);
    flipix = 2:2:size(cr_a,1);
    r_class = ones(size(cr_a,1),1);
    r_feat = zeros(size(cr_a,1), 2*size(cr_a,2));
    r_feat(noflipix,:) = [cr_a(noflipix,:), cr_u(noflipix,:)];
    r_feat(flipix,:) = [cr_u(flipix,:), cr_a(flipix,:)];
    r_class(flipix) = 2;
    %r_feat = r_feat(:,1:size(r_feat,2)/2) - r_feat(:,size(r_feat,2)/2+1:end);

    data_feat = [];
    data_feat.feat{1} = r_feat(:,:);
    data_feat.dim.feat = 'time_corr';
    data_feat.dim.time.feat{1} = mat2cell(r_class',1,ones(1,length(r_class)));
    data_feat.fsample.feat = 1/TRIALSTEP;
    data_feat.event = [];
    data_feat.cfg = {};


    %% Load classifier
    decoder = CoDecoder(['cca_classifier_',num2str(TRIALLEN(tt)),'.mat']);

    %% Evaluate classifier on test fold
    cfg = [];
    cfg.feat.test = 'all';
    [~,performance{tt}] = co_decode_classifier(cfg,data_feat,decoder);
    fprintf('\tPerformance: %d\n', performance{tt}.feat.acccurve.score(end,end));

end     % tt

%%
save(fullfile(MATBASEPATH,'performance_test.mat'), 'performance', 'fit','KERNELLEN','TRIALLEN','TRIALSTEP')