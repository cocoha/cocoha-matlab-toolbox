clear;
cd(fileparts(matlab.desktop.editor.getActiveFilename))
 %  pyenv('Version','/Users/jhjort/.pyenv/versions/matlabenv/bin/python','ExecutionMode','OutOfProcess')
MATBASEPATH = '.';
KERNELLEN = 0.5;
%TRIALLEN = [1,3,5,7,10,15,20,30] - KERNELLEN; uncomment this to test for more trial lengths
TRIALLEN = [7] - KERNELLEN;

TRIALSTEP = 1;

load('data_train.mat');

cfg = {};
cfg.eeg.cell = setdiff(1:length(data.eeg),[1]);
cfg.wavA.cell=  setdiff(1:length(data.eeg),[1]);
cfg.wavB.cell= setdiff(1:length(data.eeg),[1]);

data = co_selectcell(cfg,data);

%% Compress audio envelope
for ii = 1:length(data.wavA)
    data.wavA{ii} = max(data.wavA{ii},0).^0.3;
    data.wavB{ii} = max(data.wavB{ii},0).^0.3;
end

%% Use multismooth to filterbank EEG data
cfg = [];
cfg.eeg.kernelsizes = round(2.^unique(0:0.5:5));
data = co_multismooth(cfg,data);

cfg = [];
cfg.eeg.dim = 'smooth';
data = co_diff(cfg,data);

%% Use multismooth for audio filterbank
cfg = [];
cfg.wavA.kernelsizes = round(2.^unique(0:0.5:5));
cfg.wavB.kernelsizes = round(2.^unique(0:0.5:5));
data = co_multismooth(cfg,data);

cfg = [];
cfg.wavA.dim = 'smooth';
cfg.wavB.dim = 'smooth';
data = co_diff(cfg,data);

%% Unsplit data
cfg = [];
cfg.eeg.dim = 'time'; cfg.wavA.dim = 'time'; cfg.wavB.dim = 'time';
data = co_unsplitdata(cfg,data);

%% Pre-compute EEG/audio components
cfg = [];
cfg.wavA.method = 'pca'; cfg.wavA.segments = 30;
cfg.eeg.method = 'pca'; cfg.eeg.segments = 30;
components = co_compute_components(cfg,data);

%% Compute validation folds
NFOLDS = 10;

foldlen = ceil(size(data.eeg{1},1)/NFOLDS);
foldix = cell(1,NFOLDS);
for ii = 1:NFOLDS
    foldix{ii} = foldlen*(ii-1)+1:min(foldlen*ii,size(data.eeg{1},1));
end
foldix_all = 1:size(data.eeg{1},1);

%% Determine optimal lag and prewhitening dimension reduction
opt_lag = [];   % Assume this is the same across validation folds to save time
COMPINCR = [0, 2*9];  % Only increment channel dimension
COMPMAX = [12, size(data.eeg{1},2)*size(data.eeg{1},3)];
COMPMIN = [9, 6*9];
MAXFAIL = 2;    % Number of times change in prewhitening dimensions can occur without improvement

fit = cell(1,NFOLDS);   % Store correlation and d-prime info
performance = cell(1,NFOLDS);
for ff = 1:NFOLDS
    clear data_wt data_ccaA data_ccaB data_feat;
    currComp = COMPMIN;
    bestComp = currComp;
    bestScore = -Inf;
    failCnt = 0;

    fprintf('\tFold %d:\n',ff);
    performance{ff} = cell(1,length(TRIALLEN));
    while 1
        fprintf('\t\tTrying %d channel components. Best was %d.\n',currComp(2),bestComp(2));

%         cfg = [];
%         cfg.eeg.operation = 'whiten';
%         cfg.eeg.whitedim = {'smooth'};
%         cfg.eeg.compix = 1:currComp(1);
%         [data_wt,W_s] = co_whitendata(cfg,data);
% 
%         cfg = [];
%         cfg.eeg.operation = 'whiten';
%         cfg.eeg.whitedim = {'chan'};
%         cfg.eeg.compix = 1:currComp(2);
%         [data_wt,W_f] = co_whitendata(cfg,data_wt);

        %% PCA EEG and Audio ??
%         cfg = [];
%         cfg.wavA.method = 'pca';
%         components = co_compute_components(cfg,data_wt);

        cfg = [];
        cfg.wavA.W{1} = components.wavA.W; cfg.wavB.W{1} = components.wavA.W; cfg.eeg.W{1} = components.eeg.W;
        cfg.wavA.components{1} = 1:currComp(1); cfg.wavB.components{1} = 1:currComp(1); cfg.eeg.components{1} = 1:currComp(2);
        data_wt = co_dimreduction(cfg,data);

        %% Compute optimal lag and sum of correlations
        cfg = [];
        cfg.dim = 'time';
        cfg.input.field = 'eeg';
        cfg.output.field = 'wavA';
        cfg.method = 'cca';
        cfg.zscore = 'no';  % Data is pre-whitened for CCA
        cfg.cca.thresh = {1e-10,1e-10};
        cfg.validation.foldix = foldix(setdiff(1:length(foldix),ff));
        cfg.validation.param = 'delay';
        cfg.gpu = 'no';
        if isempty(opt_lag) % Find optimal lag if not done yet
            cfg.cca.delay = 0;
            cfg.validation.factor = 2;
            cfg.validation.maxfail = 3;
            cfg.validation.maxit = 10;
            [~,val_log] = co_train_regression(cfg,data_wt);
            opt_lag = val_log{end}.bestparam;
        else                % Still need to compute sum of correlations
            cfg.cca.delay = opt_lag;
            cfg.validation.maxfail = 1;
            cfg.validation.maxit = 1;
            [~,val_log] = co_train_regression(cfg,data_wt);
        end

        if val_log{end}.bestscore >= bestScore
            bestComp = currComp;
            bestScore = val_log{end}.bestscore;
            failCnt = 0;
        else
            failCnt = failCnt+1;
        end
        currComp = currComp + COMPINCR;
        if failCnt >= MAXFAIL || any(currComp > COMPMAX) || any(currComp < COMPMIN); break; end;

    end

    %% Whiten EEG
%     cfg = [];
%     cfg.eeg.operation = 'whiten';
%     cfg.eeg.whitedim = {'smooth'};
%     cfg.eeg.compix = 1:bestComp(1);
%     [data_wt,W_s] = co_whitendata(cfg,data);
% 
%     cfg = [];
%     cfg.eeg.operation = 'whiten';
%     cfg.eeg.whitedim = {'chan'};
%     cfg.eeg.compix = 1:bestComp(2);
%     [data_wt,W_f] = co_whitendata(cfg,data_wt);


    %% PCA Audio
%     cfg = [];
%     cfg.wavA.method = 'pca';
%     components = co_compute_components(cfg,data_wt);

    cfg = [];
    cfg.wavA.W{1} = components.wavA.W; cfg.wavB.W{1} = components.wavA.W; cfg.eeg.W{1} = components.eeg.W;
    cfg.wavA.components{1} = 1:bestComp(1); cfg.wavB.components{1} = 1:bestComp(1); cfg.eeg.components{1} = 1:bestComp(2);
    data_wt = co_dimreduction(cfg,data);

    %% Compute classifier on cross-validation folds
    cca_decoder = cell(1,length(foldix));
    for ii = 1:length(foldix)
        if ii == ff; continue; end; % Ignore test fold
        fprintf('Training fold %d...\n',ii);
        cfg = [];
        cfg.dim = 'time';
        cfg.input.field = 'eeg';
        cfg.output.field = 'wavA';
        cfg.method = 'cca';
        cfg.zscore = 'no';
        cfg.cca.delay = opt_lag;
        cfg.cca.thresh = {1e-10,1e-10};
        cfg.gpu = 'no';
        cfg.validation.foldix = {setdiff(setdiff(foldix_all,foldix{ff}),foldix{ii})};
        cca_decoder{ii} = co_train_regression(cfg,data_wt);
    end

    %% Average cross-validated decoders for test fold
    for ii = setdiff(1:length(cca_decoder),ff)
        if isempty(cca_decoder{ff})
            cca_decoder{ff} = cca_decoder{ii};
        else
            cca_decoder{ff}.decoder.A = cca_decoder{ff}.decoder.A + cca_decoder{ii}.decoder.A;
            cca_decoder{ff}.decoder.B = cca_decoder{ff}.decoder.B + cca_decoder{ii}.decoder.B;
            %cca_decoder{ff}.decoder.R = cca_decoder{ff}.decoder.R + cca_decoder{ii}.decoder.R;
        end
    end
    cca_decoder{ff}.decoder.A = cca_decoder{ff}.decoder.A/(length(cca_decoder)-1);
    cca_decoder{ff}.decoder.B = cca_decoder{ff}.decoder.B/(length(cca_decoder)-1);
    %cca_decoder{ff}.decoder.R = cca_decoder{ff}.decoder.R/(length(cca_decoder)-1);

    %% Split data into folds
    cfg = [];
    cfg.eeg.dim = 'time'; cfg.wavA.dim = 'time'; cfg.wavB.dim = 'time';
    cfg.eeg.splitsample = zeros(1,length(foldix)-1);
    for ii = 2:length(foldix); cfg.eeg.splitsample(ii-1) = foldix{ii}(1); end;
    cfg.wavA.splitsample = cfg.eeg.splitsample; cfg.wavB.splitsample = cfg.eeg.splitsample;
    data_wt = co_splitdata(cfg,data_wt);

    %% Evaluate each cross-validation fold
    % Attended
    data_ccaA = cell(1,length(foldix));
    for ii = 1:length(foldix)
        cfg = [];
        cfg.input.field = 'eeg';
        cfg.input.cell = ii;
        cfg.output.field = 'wavA';
        data_ccaA{ii} = co_decode_regression(cfg,data_wt,cca_decoder{ff});  % Use averaged decoder??
    end

    % Unattended
    data_ccaB = cell(1,length(foldix));
    for ii = 1:length(foldix)
        cfg = [];
        cfg.input.field = 'eeg';
        cfg.input.cell = ii;
        cfg.output.field = 'wavB';
        data_ccaB{ii} = co_decode_regression(cfg,data_wt,cca_decoder{ff});
    end

    %% Compute test correlations (also store hyperparameters)
    fit{ff}.corrA = nt_normcol(data_ccaA{ff}.eeg{1})' * nt_normcol(data_ccaA{ff}.wavA{1}) ./ length(data_ccaA{ff}.eeg{1});
    fit{ff}.corrB = nt_normcol(data_ccaB{ff}.eeg{1})' * nt_normcol(data_ccaB{ff}.wavB{1}) ./ length(data_ccaB{ff}.eeg{1});
    fit{ff}.opt_lag = opt_lag;
    fit{ff}.bestComp = bestComp;

    for tt = 1:length(TRIALLEN)
        fprintf('\t\t\tTime window: %ds\n',TRIALLEN(tt));
        %% Compute attended features (Attended decoder, attended audio)
        cr_a = [];
        testix = zeros(1,2);
        for ii = 1:length(data_ccaA)
            if ii == ff; testix(1) = length(cr_a)+1; end;
            for jj = 1:TRIALSTEP*data.fsample.eeg:size(data_ccaA{ii}.eeg{1},1)-TRIALLEN(tt)*data.fsample.eeg;
                ix = jj:jj+TRIALLEN(tt)*data.fsample.eeg-1-opt_lag;
                r = nt_normcol(data_ccaA{ii}.eeg{1}(ix,:))' * nt_normcol(data_ccaA{ii}.wavA{1}(ix,:)) ./ length(ix);
                if isempty(cr_a)
                    cr_a = [diag(r)]';
                else
                     cr_a = [cr_a; diag(r)'];
                end
            end
            if ii == ff; testix(2) = length(cr_a); end;
        end
        testix = testix(1):testix(2);

        %% Compute unattended features (Attended decoder, unattended audio)
        cr_u = [];
        for ii = 1:length(data_ccaB)
            for jj = 1:TRIALSTEP*data.fsample.eeg:size(data_ccaB{ii}.eeg{1},1)-TRIALLEN(tt)*data.fsample.eeg
                ix = jj:jj+TRIALLEN(tt)*data.fsample.eeg-1-opt_lag;
                r = nt_normcol(data_ccaB{ii}.eeg{1}(ix,:))' * nt_normcol(data_ccaB{ii}.wavB{1}(ix,:)) ./ length(ix);
                if isempty(cr_u)
                    cr_u = [diag(r)]';
                else
                    cr_u = [cr_u; diag(r)'];
                end
            end
        end

        %% Compute d-prime (for current triallen)
        fit{ff}.dprime{tt} = (mean(cr_a(testix,:),1) - mean(cr_u(testix,:),1))./sqrt(0.5*(var(cr_a(testix,:),[],1)+var(cr_u(testix,:),[],1)));

        %% Create combined detection dataset
        noflipix = 1:2:size(cr_a,1);
        flipix = 2:2:size(cr_a,1);
        r_class = ones(size(cr_a,1),1);
        r_feat = zeros(size(cr_a,1), 2*size(cr_a,2));
        r_feat(noflipix,:) = [cr_a(noflipix,:), cr_u(noflipix,:)];
        r_feat(flipix,:) = [cr_u(flipix,:), cr_a(flipix,:)];
        r_class(flipix) = 2;
        %r_feat = r_feat(:,1:size(r_feat,2)/2) - r_feat(:,size(r_feat,2)/2+1:end);

        data_feat = [];
        data_feat.feat{1} = r_feat(:,:);
        data_feat.dim.feat = 'time_corr';
        data_feat.dim.time.feat{1} = mat2cell(r_class',1,ones(1,length(r_class)));
        data_feat.fsample.feat = 1/TRIALSTEP;
        data_feat.event = [];
        data_feat.cfg = {};


        %% Train classifier on cross-validation folds
        cfg = [];
        cfg.feat.dim = 'time';
        cfg.feat.train = setdiff(1:size(data_feat.feat{1},1),testix);
        cfg.feat.classalgo = 'pylogit';
        cfg.feat.pysvm.opt = {'kernel','linear'};
        cfg.feat.svm.svmopt = {'KernelFunction','linear'};
        decoder = co_train_classifier(cfg,data_feat);

        %% Evaluate classifier on test fold
        cfg = [];
        cfg.feat.test = testix;
        [~,performance{ff}{tt}] = co_decode_classifier(cfg,data_feat,decoder);
        fprintf('\t\t\t\tPerformance: %d\n', performance{ff}{tt}.feat.acccurve.score(end,end));

    end     % tt
end     % ff
%% Save best parameters, fit and performance
save(fullfile(MATBASEPATH,'performance_train.mat'), 'performance', 'fit','KERNELLEN','TRIALLEN','TRIALSTEP')