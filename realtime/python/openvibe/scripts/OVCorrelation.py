# -*- coding: utf-8 -*-
"""
OVCorrelation computes the correlations between the 1st half of the channels and the 2nd half.

The OpenViBE I/O types are:
Input 0             = signal
Output 0            = signal

The following settings need to be set in OpenViBE:
'Demean'            = (boolean) whether or not to demean when computing correlation.
'Epoch Length'      = (integer) length window used to compute correlation (in samples).
'Sampling Rate'     = (integer) sampling rate of the output.
'Samples Per Chunk' = (integer) number of samples per chunk in the output.

Created on Wed Aug 30 12:00:01 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import math;
import numpy as np;
from scipy.stats import pearsonr;

corrNorm = lambda x: x/np.sqrt(np.sum(x**2)/x.size);
corr2 = lambda x, y: np.sum(corrNorm(x)*corrNorm(y))/x.size;

class OVCorrelation(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.lastTime = 0;                      # Time of last received sample
        
        self.demean = True;                     # If true, use standard Pearson Correlation, otherwise use corr2
        self.epochLen = 0;                      # Size of window used to compute correlation
        self.newfs = 0;                         # Sampling rate of correlation output
        self.samplesPerChunk = 1;               # Number of samples per chunk in output
        self.stepSize = 0;                      # Size of output time steps in terms of input samples
        self.dataBuffer = None;                 # Used to store data to achieve epoch size
        self.bufferTimes = None;                # Times of samples in buffer
    
    def initialize(self):
        if self.setting['Demean'] in ['False','false']:
            self.demean = False;
        self.epochLen = int(self.setting['Epoch Length']);
        self.newfs = int(self.setting['Sampling Rate']);
        self.samplesPerChunk = int(self.setting['Samples Per Chunk']);
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                self.signalHeaderOut.samplingRate = self.newfs;
                self.stepSize = int(float(self.signalHeaderIn.samplingRate)/self.newfs);
                
                self.signalHeaderOut.dimensionSizes[0] /= 2;
                self.signalHeaderOut.dimensionSizes[-1] = self.samplesPerChunk;
                del self.signalHeaderOut.dimensionLabels[ \
                    :int(math.ceil(self.signalHeaderIn.dimensionSizes[0]/2))];
                
                self.dataBuffer = np.zeros((self.signalHeaderIn.dimensionSizes[0], self.epochLen));
                self.bufferTimes = np.arange(-self.epochLen/float(self.signalHeaderIn.samplingRate), \
                    0., 1./self.signalHeaderIn.samplingRate);
                
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                
                # Append to data buffer
                data = np.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes);
                self.dataBuffer = np.concatenate((self.dataBuffer,data), axis=-1);
                
                # Beginning time of each sample
                t = np.arange(chunk.startTime, chunk.endTime, \
                        (chunk.endTime-chunk.startTime)/self.signalHeaderIn.dimensionSizes[-1]);
                self.bufferTimes = np.append(self.bufferTimes,t);
                
                if self.dataBuffer.shape[-1] - self.epochLen >= \
                        self.stepSize*self.samplesPerChunk:
                    # Enough data gathered to put out next chunk
                    dataOut = np.zeros(self.signalHeaderOut.dimensionSizes);
                    startTime = self.bufferTimes[-int(self.stepSize)*self.samplesPerChunk];
                    endTime = self.bufferTimes[-1];
                    for i in range(self.signalHeaderOut.dimensionSizes[-1]):
                        for j in range(self.signalHeaderOut.dimensionSizes[0]):
                            if self.demean:
                                dataOut[j,i],_ = pearsonr(self.dataBuffer[j,i*self.stepSize:self.epochLen+i*self.stepSize], \
                                    self.dataBuffer[j+self.signalHeaderOut.dimensionSizes[0],i*self.stepSize:self.epochLen+i*self.stepSize]);
                            else:
                                dataOut[j,i] = corr2(self.dataBuffer[j,i*self.stepSize:self.epochLen+i*self.stepSize], \
                                    self.dataBuffer[j+self.signalHeaderOut.dimensionSizes[0],i*self.stepSize:self.epochLen+i*self.stepSize]);
                    dataOut = np.nan_to_num(dataOut);
                    
                    # Trim buffers
                    self.dataBuffer = self.dataBuffer[:,-self.epochLen:];
                    self.bufferTimes = self.bufferTimes[-self.epochLen:];
                        
                    chunk = OVSignalBuffer(startTime, endTime, dataOut.flatten().tolist());
                    self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));

box = OVCorrelation();