"""
OVStateTracker uses information transfer rate to switch between two states represented by +/-1.

The OpenViBE I/O types are:
Input 0                 = signal, classifier probability (single channel)
Output 0                = signal, state between +/-1 (single channel)

The following settings need to be set in OpenViBE:
'Epoch Length'          = (float) epoch length in seconds.
'Half life'             = (float) state half life in seconds.
'Gain'                  = (float) gain to apply to the ITR.

Created on Thu Jul 5 12:00:01 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy
import numpy as np
from scipy.stats import norm

NCLASS = 2;

class OVStateTracker(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.state = 0.;
        self.samplingRate = np.Inf;
        self.signalHeaderIn = None;
        self.signalHeaderOut = None;
        self.epochLen = np.Inf;
        self.exponent = 1.;
        self.lmb = np.Inf;
    
    def initialize(self):
        self.epochLen = float(self.setting['Epoch Length']);
        self.lmb = float(self.setting['Lambda'])
        self.exponent = float(self.setting['Exponent']);
    
    def process(self):
        for chunkIndex in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                self.signalHeaderIn = self.input[0].pop();
                self.samplingRate = self.signalHeaderIn.samplingRate;
                assert self.signalHeaderIn.dimensionSizes[0] <= 2, 'Input must contain at most 2 channels';
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                self.signalHeaderOut.dimensionSizes[0] = 1;
                self.signalHeaderOut.dimensionLabels = ['state'] + ['']*self.signalHeaderOut.dimensionSizes[1];
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIndex]) == OVSignalBuffer: # Perform thresholding
                chunk = self.input[0].pop();
                data = np.array(chunk).reshape(self.signalHeaderIn.dimensionSizes);
                #if data.shape[0] == 2:
                #    data[0,:] = norm.cdf(norm.ppf(data[0,:])+0.5*norm.ppf(data[1,:]/2+0.5))
                itr = (np.log2(NCLASS) + data[0,:]*np.log2(data[0,:]) + \
                    (1-data[0,:])*np.log2((1-data[0,:])/(NCLASS-1)))  # Bits per epoch
                itr = itr.flatten();
                itr[np.isnan(itr)] = 1.
    
                state = np.zeros((self.signalHeaderOut.dimensionSizes[1],));
                lmb = self.lmb;
                #exponent = self.exponent;
                if data.shape[0] == 2:
                    lmb *= abs(self.state - data[1,0]);
                    #exponent = max(16.,exponent/abs(self.state - data[1,0]))
                state[0] = self.state + \
                    (itr[0]*np.sign(data[0,0]-0.5)-lmb*abs(self.state)**self.exponent*np.sign(self.state)) \
                    *2/self.samplingRate/self.epochLen;
                if state[0] > 1:
                    state[0] = 1.
                elif state[0] < -1:
                    state[0] = -1.;
                for i in range(1,len(itr)):
                    lmb = self.lmb;
                    #exponent = self.exponent
                    if data.shape[0] == 2:
                        lmb *= abs(state[i-1] - data[1,i]);
                        #exponent = max(16.,exponent/abs(state[i-1] - data[1,i]))
                    state[i] = state[i-1] + \
                        (itr[i]*np.sign(data[0,i]-0.5)-lmb*abs(state[i-1])**self.exponent*np.sign(state[i-1])) \
                        *2/self.samplingRate/self.epochLen;
                    if state[i] > 1:
                        state[i] = 1.
                    elif state[i] < -1:
                        state[i] = -1.;
                
                self.state = state[-1];
            
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, state.tolist());
                self.output[0].append(chunk);
                self.lastTime = chunk.endTime;
            else:
                    # OVSignalEnd - forward to output
                    self.output[0].append(self.input[0].pop());
        
        def uninitialize(self):
            self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));

box = OVStateTracker();