# -*- coding: utf-8 -*-
"""
OVStreamSync synchronizes streams by waiting until a predefined stimulation signal has been
received from the incoming stream before outputting its signal. The signal and stimulation times are
altered so that the stimulation signal and the corresponding signal sample start at time 0.

This box is meant as a substitute for the native Stream Synchronization box (which I haven't got
working).

The OpenViBE I/O types are:
Input 0             = signal
Input 1             = stimulations
Output 0            = signal
Output 1            = stimulations

The following settings need to be set in OpenViBE:
'Stimulation'       = (stimulation) OpenViBE stimulation label of start stimulus
'Max Buffer Time'   = (float) Maximum length of data buffer (s). The start stimulation should not
                      arrive at this OVBox earlier than its corresponding signal sample by this
                      amount of time.

Created on Mon Nov  9 15:34:50 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy;
import warnings;

class OVStreamSync(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.stimLabel = None;                  # OpenViBE stimulation label
        self.stimCode = None;                   # Code corresponding to stimulation label
        
        self.signalHeader = None;
        self.dataBufferTMax = None;             # Maximum length of data buffer (s)
        self.dataBufferLenMax = None;           # Maximum number of time samples in data buffer
        self.dataBuffer = numpy.array([]);
        self.startTimes = numpy.array([]);      # Start times of each sample in data buffer
        self.delay = None;                      # Amount of time to delay signals: start stimulus arrival time
        self.offset = None;                     # Offset between delayed first chunk start time and 0
        self.lastTime = 0;                      # End time of last sent chunk (used for uninitialize)
    
    def initialize(self):
        self.stimLabel = self.setting['Stimulation'];
        self.stimCode = OpenViBE_stimulation[self.stimLabel];
        self.dataBufferTMax = float(self.setting['Max Buffer Time']);
        
    def process(self):
        # Process signals
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop();
                self.output[0].append(self.signalHeader);
                self.dataBufferLenMax = int(self.signalHeader.samplingRate*self.dataBufferTMax);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                
                # Automatically copy data to buffer
                data = numpy.array(chunk);
                data = data.reshape(self.signalHeader.dimensionSizes);
                self.dataBufferAppend(data);
                
                # Beginning time of each sample
                t = numpy.arange(chunk.startTime, chunk.endTime, \
                        (chunk.endTime-chunk.startTime)/self.signalHeader.dimensionSizes[1]);
                self.startTimes = numpy.append(self.startTimes,t);
                
                if self.delay is None:
                    # Ensure buffer size doesn't exceed maximum limit
                    if self.dataBuffer.shape[1] > self.dataBufferLenMax:
                        self.dataBuffer = self.dataBuffer[:,-self.dataBufferLenMax:];
                        self.startTimes = self.startTimes[-self.dataBufferLenMax:];
                
                if self.delay is not None and self.offset is None:      # Delay detected, but no sample times > delay found yet
                    tkeep_ix = numpy.where(self.startTimes>=self.delay)[0];
                    if len(tkeep_ix) > 0:                               # Sample times > delay found
                        self.dataBuffer = self.dataBuffer[:,tkeep_ix];  # Remove time samples before start stimulus
                        self.startTimes = self.startTimes[tkeep_ix];
                        self.offset = self.startTimes[0] - self.delay;  # Compute offset so first sent sample starts at time 0
                        if self.offset > self.dataBufferTMax:
                            warnings.warn("Start stimulus arrived {}s ".format(self.offset) + \
                                    "earlier than the corresponding signal sample. Consider " + \
                                    "increasing the maximum buffer time.");
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
        
        # See if there are samples available to be sent
        # self.offset doubles as a flag to indicate pre-stim samples removed from buffer
        if self.offset is not None and len(self.startTimes) > 0:
            nSamples = self.signalHeader.dimensionSizes[1];     # Number of samples per output chunk
            nOutChunks = (self.dataBuffer.shape[1]-1)/nSamples; # Need one sample left in buffer so we know start time of next chunk
            for chunkIx in range(nOutChunks):
                data = self.dataBuffer[:,chunkIx*nSamples:(chunkIx+1)*nSamples];
                startTime = self.startTimes[chunkIx*nSamples] - self.delay - self.offset;
                endTime = self.startTimes[(chunkIx+1)*nSamples] - self.delay - self.offset;     # End time is start time of next chunk
                self.lastTime = endTime;                                                        # Keep track of end time for uninitialize
                chunk = OVSignalBuffer(startTime, endTime, data.flatten().tolist());
                self.output[0].append(chunk);
            
            # Remove sent data from buffers
            self.startTimes = self.startTimes[nOutChunks*nSamples:];
            self.dataBuffer = self.dataBuffer[:,nOutChunks*nSamples:];
        
        # Search stimulations for start
        for chunkIx in range(len(self.input[1]))[::-1]:
            if type(self.input[1][chunkIx]) == OVStimulationHeader:
                self.output[1].append(self.input[1].pop());
            else:   # OVStimulationSet
                stimSet = self.input[1].pop();
                stimSetOut = OVStimulationSet(stimSet.startTime, stimSet.endTime);
                
                for stimIx in range(len(stimSet)):
                    if self.delay is None and stimSet[stimIx].identifier == self.stimCode:
                        self.delay = stimSet[stimIx].date;
                    
                    if self.delay is not None:
                        stim = stimSet[stimIx];
                        stim.date -= self.delay;
                        stimSetOut.append(stim);
                
                if len(stimSetOut) > 0:
                    self.output[1].append(stimSetOut);
    
    # Manages appending of data to buffer (handles case where buffer is empty)
    def dataBufferAppend(self, x):
        if len(self.dataBuffer) == 0:
            self.dataBuffer = x;
        else:
            self.dataBuffer = numpy.concatenate((self.dataBuffer,x),axis=1);
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime,self.lastTime));

box = OVStreamSync();