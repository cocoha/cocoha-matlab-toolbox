# -*- coding: utf-8 -*-
"""
OVDELAY delays an input signal by a specified number of samples.

The OpenViBE I/O types are:
Input 0             = signal
Output 0            = signal

The following settings need to be set in OpenViBE:
'Delay'             = (integer) the delay in number of samples.

Created on Thu Sep  7 18:14:04 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy as np;
from scipy.signal import lfilter;

class OVDelay(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;               # Input signal header
        self.lastTime = 0.;                     # Time of last received sample
        self.delay = 0;                         # Delay in samples
        self.delayTime = None;                  # Delay in time for each input
        self.zi = None;                         # Initial conditions for delay filter
        self.kernel = None;                     # Delay kernel
    
    def initialize(self):
        self.signalHeader = [[]]*len(self.input);
        self.delay = int(self.setting['Delay']);
        self.delayTime = [0.]*len(self.input);
        self.zi = [[]]*len(self.input);
        self.kernel = np.zeros((self.delay+1,));
        self.kernel[-1] = 1.;
        
    def process(self):
        for inIx in range(len(self.input)):
            for chunkIx in range(len(self.input[inIx]))[::-1]:
                if type(self.input[inIx][chunkIx]) == OVSignalHeader:
                    # Pass signal header to output
                    self.signalHeader[inIx] = self.input[inIx].pop();
                    self.output[inIx].append(self.signalHeader[inIx]);
                    
                    self.delayTime[inIx] = float(self.delay)/self.signalHeader[inIx].samplingRate;
                    
                    # Create filter initial conditions
                    for i in range(self.signalHeader[inIx].dimensionSizes[0]):
                        self.zi[inIx].append(np.zeros((self.delay,)));
                elif type(self.input[inIx][chunkIx]) == OVSignalBuffer:
                    chunk = self.input[inIx].pop();
                    self.lastTime = chunk.endTime;
                    data = np.array(chunk);
                    data = data.reshape(self.signalHeader[inIx].dimensionSizes);
                    
                    data,self.zi[inIx] = lfilter(self.kernel,[1.],data,zi=self.zi[inIx]);
                    chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, data.flatten().tolist());
                    self.output[inIx].append(chunk);
                elif type(self.input[inIx][chunkIx]) == OVStimulationSet:
                    stimSet = self.input[inIx].pop();
                    stimSetOut = OVStimulationSet(stimSet.startTime-self.delayTime, \
                        stimSet.endTime-self.delayTime);
                    
                    for stimIx in range(len(stimSet)):
                        stim = stimSet[stimIx];
                        stim.date -= self.delayTime;
                        stimSetOut.append(stim);
                    
                    self.output[inIx].append(stimSetOut);
                else:
                    # OVSignalEnd - forward to output
                    self.output[inIx].append(self.input[inIx].pop());
    
    def uninitialize(self):
        for i in range(len(self.input)):
            self.output[i].append(OVSignalEnd(self.lastTime,self.lastTime));

box = OVDelay();