# -*- coding: utf-8 -*-
"""
OVMatrixMultiply multiplies a 2D signal with M channels by an MxN matrix saved to a specified
Matlab file under a variable named 'mat'. The new N channels can also be specified as a Matlab cell
array named 'chan'.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Mat File'      = (string) name of Matlab file containing matrix.

Created on Tue Aug 29 15:09:44 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import numpy as np;
import scipy.io as scpio;

class OVMatrixMultiply(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.lastTime = 0;
        self.matFile = None;
        self.mat = None;
        self.chan = [];
    
    def initialize(self):
        self.matFile = self.setting['Mat File'];
        mat = scpio.loadmat(self.matFile);
        self.mat = mat['mat'].transpose();
        self.chan = [];
        if 'chan' not in mat:
            self.chan = ['Mult' + str(i) for i in range(0,self.mat.shape[0])];
        else:
            for i in range(0,mat['chan'].shape[1]):
                self.chan.append(str(mat['chan'][0][i][0]));
        assert len(self.mat.shape) == 2, 'mat variable must be 2D.';
        assert len(self.chan) == self.mat.shape[0], \
            'Number of new channel labels does not match size of 2nd mat dimension.';
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                
                assert len(self.signalHeaderIn.dimensionSizes) == 2, \
                    'Only 2D signals supported by OVMatrixMultiply.';
                
                self.signalHeaderOut.dimensionSizes[0] = len(self.chan);
                self.signalHeaderOut.dimensionLabels = self.chan + \
                    ['']*self.signalHeaderOut.dimensionSizes[1];
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes);
                data = np.dot(self.mat,data);
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, data.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVMatrixMultiply();
        