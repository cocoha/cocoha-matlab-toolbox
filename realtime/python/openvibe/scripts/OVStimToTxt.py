# -*- coding: utf-8 -*-
"""
OVStimToTxt writes stimulations to a file, with each line indicating the time (s) and the
OpenViBE name of the stimulus, separated by a comma. This is useful when reading data files saved
by OpenViBE using a 3rd-party program that may not necessarily read in the stimulations properly.

The OpenViBE I/O types are:
Input 0                 = stimulations


Created on Mon Sep 11 16:55:28 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

class OVStimToTxt(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.f = None;
        self.rev_ref = dict((v,k) for k,v in OpenViBE_stimulation.iteritems());
    
    def initialize(self):
        self.f = open(self.setting['Filename'],'w');
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVStimulationSet:
                stimSet = self.input[0].pop();
                for stimIx in range(len(stimSet)):
                    try:
                        self.f.write(str(stimSet[stimIx].date) + ',' + \
                            self.rev_ref[stimSet[stimIx].identifier] + '\n');
                    except KeyError:
                        self.f.write(str(stimSet[stimIx].date) + ',' + \
                            str(stimSet[stimIx].identifier) + '\n');
            else:
                self.input[0].pop();    # Discard (OVStimulationHeader)
    
    def uninitialize(self):
        self.f.close();

box = OVStimToTxt();