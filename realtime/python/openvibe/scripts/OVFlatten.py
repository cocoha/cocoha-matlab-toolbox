# -*- coding: utf-8 -*-
"""
OVFlatten flattens the non-time dimensions of the input signal.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Order'    = (string) 'C' for flattening in a row-major (C-style) order. 'F' for flattening in a
             column-major (Fortran-style) order. Note that Matlab uses a column major order.

Created on Tue Aug 29 11:10:22 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy as np;
import copy;

class OVFlatten(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.lastTime = 0;
        self.order = 'F';
    
    def initialize(self):
        self.order = self.setting['Order'];
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                self.signalHeaderOut.dimensionSizes = \
                    [np.prod(self.signalHeaderIn.dimensionSizes[:-1]), \
                    self.signalHeaderIn.dimensionSizes[-1]];
                self.signalHeaderOut.dimensionLabels = [];
                
                dimensionLabels = [];
                ix = 0;
                for i in range(len(self.signalHeaderIn.dimensionSizes)-1):
                    dimensionLabels.append(self.signalHeaderIn.dimensionLabels[ \
                        ix:ix+self.signalHeaderIn.dimensionSizes[i]]);
                    ix = ix + self.signalHeaderIn.dimensionSizes[i];
                
                for i in range(self.signalHeaderOut.dimensionSizes[0]):
                    unravel_coords = np.unravel_index(i, \
                        tuple(self.signalHeaderIn.dimensionSizes[:-1]), order=self.order);
                    label = '';
                    for j in range(len(unravel_coords)):
                        label += dimensionLabels[j][unravel_coords[j]];
                    self.signalHeaderOut.dimensionLabels.append(label);
                self.signalHeaderOut.dimensionLabels += ['']*self.signalHeaderIn.dimensionSizes[-1];
                
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                # Append incoming data to buffer
                chunk = self.input[0].pop();
                if self.order == 'F':   # Order is only applied to non-time dimensions
                    data = np.array(chunk);
                    data = data.reshape(self.signalHeaderIn.dimensionSizes);
                    data = np.rollaxis(data,-1);
                    dataOut = np.zeros(self.signalHeaderOut.dimensionSizes);
                    for j in range(0,dataOut.shape[-1]):
                        dataOut[:,j] = data[j,].flatten('F');
                    chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, \
                        dataOut.flatten().tolist());
                self.lastTime = chunk.endTime;
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVFlatten();