# -*- coding: utf-8 -*-
"""
OVTEMPORALFILTER allows for filtering of the data with forward-pass filtering. It currently
zero-pads the initial conditions and removes an offset computed from time-average of data in the
first chunk. This could be improved by performing some form of detrending and/or optimizing the
initial conditions.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Filter Method'         = (string) can be 'butter' or 'fir'.
'Filter Type'           = (string) can be 'highpass', 'bandpass', 'lowpass', or 'bandstop'.
'Order'                 = (integer) order of the filter.
'Critical Frequencies'  = (string) a comma separated list of critical frequencies for the filter.
                          See SciPy documentation for more information.

Created on Fri Sep 15 11:58:08 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy as np;
from scipy import signal;

class OVTemporalFilter(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;
        self.lastTime = 0.
        self.fmethod = 'butter';
        self.ftype = 'highpass';
        self.order = 0;
        self.critFreq = 0.;
        self.b = [1.];
        self.a = [1.];
        self.zi = [];
        self.offset = None;
    
    def initialize(self):
        self.fmethod = self.setting['Filter Method'];
        self.ftype = self.setting['Filter Type'];
        self.order = int(self.setting['Order']);
        self.critFreq = np.array([float(x) for x in self.setting['Critical Frequencies'].split(',')]);
        if len(self.critFreq) == 1:
            self.critFreq = self.critFreq[0];
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop();
                self.output[0].append(self.signalHeader);
                
                if self.fmethod == 'butter':
                    self.b,self.a = signal.butter(self.order, \
                        self.critFreq/self.signalHeader.samplingRate*2, self.ftype);
                elif self.fmethod == 'fir':
                    if self.ftype in ['highpass','bandpass']:
                        pass_zero = False;
                    elif self.ftype in ['lowpass','bandstop']:
                        pass_zero = True;
                    self.b = signal.firwin(self.order+1, \
                        self.critFreq/self.signalHeader.samplingRate*2, pass_zero=pass_zero);
                else:
                    raise ValueError('Unrecognized filter type.');
                
                for i in range(self.signalHeader.dimensionSizes[0]):
                    self.zi.append(np.zeros((max(len(self.a),len(self.b))-1,)));
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeader.dimensionSizes);
                if self.offset is None:
                    self.offset = np.mean(data, axis=-1, keepdims=True);
                data -= self.offset;
                data,self.zi = signal.lfilter(self.b, self.a, data, zi=self.zi);
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, data.flatten().tolist());
                self.output[0].append(chunk);
            elif type(self.input[0][chunkIx]) == OVStimulationSet:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVTemporalFilter();