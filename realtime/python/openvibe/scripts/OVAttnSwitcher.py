# -*- coding: utf-8 -*-
"""
OVATTNSWITCHER simulates attention switching by swapping two channels (e.g. audio envelopes) every pre-
defined time interval within a trial.

The OpenViBE I/O types are:
Input 0             = signal
Input 1             = stimulations
Output 0            = signal
Output 1            = stimulations

The following settings need to be set in OpenViBE:
Start Stimulations  = (string) a comma separated list of OpenVibe stimulation names to be detected in the
                      input, indicating the start of a trial.
Output Type         = (string) 'class' or 'decision'. Determines whether to output the class or the
                      decision function.


Created on Fri Sep 14 16:00:25 2018

@author: dwong
"""

import numpy as np


class OVAttnSwitcher(OVBox):
    def __init__(self):
        OVBox.__init__(self)
        self.trialStartStims = []
        self.trialEndStims = []
        self.switchChannels = []
        self.switchStims = []
        self.interval = 1.
        self.signalHeader = None
        self.trialFlag = -2         # indexes 0-N indicate which trial start stim is active. Index -1 indicates EndStim received. Index -2 indicates EndStim sent.
        self.trialStart = 0.
        self.trialEnd = np.Inf
        self.switchFlag = False
        self.lastTime = 0.
    
    def initialize(self):
        self.trialStartStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['Start Stimulations'].split(',')]
        self.trialEndStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['End Stimulations'].split(',')]
        self.switchChannels = [int(i) for i in self.setting['Switch Channels'].split(',')]
        self.switchStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['Switch Stimulations'].split(',')]
        self.interval = float(self.setting['Interval'])
    
    def process(self):
        for chunkIx in range(len(self.input[1])):
            chunk = self.input[1].pop()
            if type(chunk) == OVStimulationSet:
                for stimix in range(len(chunk)):
                    stim = chunk.pop()
                    if stim.identifier in self.trialEndStims:
                        self.trialFlag = -1
                        self.trialEnd = stim.date
                        #print 'End Stim'
                    elif stim.identifier in self.trialStartStims:
                        ix = self.trialStartStims.index(stim.identifier)
                        self.trialFlag = ix
                        self.trialStart = stim.date
                        self.trialEnd = np.Inf
                        self.switchFlag = True
            else:
                self.output[1].append(chunk)
        
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop()
                self.output[0].append(self.signalHeader)
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop()
                self.lastTime = chunk.endTime
                data = np.array(chunk)
                data = data.reshape(self.signalHeader.dimensionSizes)
                
                stim = None
                if self.trialFlag >= 0 and chunk.endTime < self.trialEnd:
                    if ((chunk.startTime - self.trialStart) // self.interval) % 2 == 1:
                        #switchTime = ((chunk.startTime - self.trialStart) // self.interval + 1) * self.interval
                        #switchProportion = self.interval / (chunk.endTime - switchTime)
                        data[self.switchChannels,] = data[self.switchChannels[::-1],]   # Swap channels
                        if not self.switchFlag:
                            self.switchFlag = True
                            stim = OVStimulation(self.switchStims[self.trialFlag],chunk.startTime,0.)
                            #print 'switch1'
                    else:
                        if self.switchFlag:
                            self.switchFlag = False
                            stim = OVStimulation(self.trialStartStims[self.trialFlag],chunk.startTime,0.)
                            #print 'switch2'
#                
                # Send signal
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, \
                    data.flatten().tolist());
                self.output[0].append(chunk);
#                
#                # Send stimulations
                stimSet = OVStimulationSet(chunk.startTime, chunk.endTime)
                if self.trialFlag == -1:
                    stimSet.append(OVStimulation(self.trialEndStims[0],chunk.endTime,0.))
                    self.trialFlag = -2
                elif stim:
                    stimSet.append(stim)
                
                self.output[1].append(stimSet)
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop())
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime))

box = OVAttnSwitcher()