# -*- coding: utf-8 -*-
"""
OVWhiten allows a whitening matrix to be applied to one or more (non-time) dimensions, using the
scheme in CO_WHITENDATA.

The OpenViBE I/O types are:
Input               = Signal
Output              = Signal

The following settings need to be set in OpenViBE:
'Mat File'          = (Filename) Path to Matlab .mat file containing a 'mat' variable that stores
                      W.FIELD.P.
'Dimension'         = (String) A comma-delimited list of dimension indexes to whiten.
'Components'        = (String) A comma and dash-delimited list of whitening components.

Created on Fri Sep  8 10:44:54 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy
import numpy as np;
import scipy.io as scpio;

class OVWhiten(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;
        self.signalHeaderOut = None;
        self.lastTime = 0.;
        self.mat = None;
        self.whiteDim = [0];
        self.components = [];
    
    def initialize(self):
        self.mat = scpio.loadmat(self.setting["Mat File"])['mat'];
        self.whiteDim = self.setting["Dimension"].split(',');
        self.whiteDim = [int(x) for x in self.whiteDim];
        self.components = parseRange(self.setting["Components"]);
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                self.signalHeaderOut.dimensionSizes[self.whiteDim[0]] = len(self.components);
                for i in range(len(self.whiteDim)-1):
                    self.signalHeaderOut.dimensionSizes.insert(self.whiteDim[i],1);
                
                self.signalHeaderOut.dimensionLabels = [];
                dimensionLabels = [];
                ix = 0;
                for i in range(len(self.signalHeaderIn.dimensionSizes)):
                    dimensionLabels.append(self.signalHeaderIn.dimensionLabels[ \
                        ix:ix+self.signalHeaderIn.dimensionSizes[i]]);
                    ix = ix + self.signalHeaderIn.dimensionSizes[i];
                
                for i in range(len(self.signalHeaderOut.dimensionSizes)):
                    if i == self.whiteDim[0]:
                        self.signalHeaderOut.dimensionLabels += \
                            ["component" + str(j) for j in range(len(self.components))];
                    elif i in self.whiteDim:
                        self.signalHeaderOut.dimensionLabels += dimensionLabels[i][0];
                    else:   # nonwhitedim or time
                        self.signalHeaderOut.dimensionLabels += dimensionLabels[i];

                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes);
                
                # Perform whitening
                sz = self.signalHeaderIn.dimensionSizes;
                alldims = range(len(data.shape));
                tdim = alldims[-1];
                nonwhiteDim = np.setdiff1d(np.setdiff1d(alldims,[tdim]),self.whiteDim);
                tp = np.concatenate(([tdim],self.whiteDim,nonwhiteDim));
                data = data.transpose(tp);
                data = data.reshape((sz[tdim],np.prod([sz[i] for i in self.whiteDim]), \
                    np.prod([sz[i] for i in nonwhiteDim])), order='F');
                dataOut = np.zeros((data.shape[0],len(self.components),data.shape[2]));
                for i in range(data.shape[2]):
                    dataOut[:,:,i] = np.dot(data[:,:,i],self.mat[:,self.components]);
                dataOut = dataOut.reshape([sz[tdim], len(self.components)] + \
                    [1]*(len(self.whiteDim)-1) + \
                    [np.prod(sz[nonwhiteDim])], order='F');
                dataOut = dataOut.transpose(np.argsort(tp));
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, dataOut.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        

"""
Parses a string of numbers separated by commas and dashes into a list of integers.
Taken from:
https://stackoverflow.com/questions/6405208/how-to-convert-numeric-string-ranges-to-a-list-in-python
"""
def parseRange(x):
    result = [];
    for part in x.split(','):
        if '-' in part:
            a, b = part.split('-');
            a, b = int(a), int(b);
            result.extend(range(a, b + 1));
        else:
            a = int(part);
            result.append(a);
    return result;

box = OVWhiten();