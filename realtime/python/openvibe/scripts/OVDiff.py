# -*- coding: utf-8 -*-
"""
OVDiff differences a signal along a specified axis. This axis should not be the time axis.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Axis'    = (integer) axis on which to perform diff operation.
'Zero Pad'= (boolean) whether or not to add zeros at the beginning of the differenced axis to
            maintain size.

Created on Tue Aug 29 15:21:04 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import numpy as np;

class OVDiff(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.lastTime = 0;                      # Time of last received sample
        self.axis = 0;                          # Axis on which to perform diff
        self.zeropad = True;
        self.zeros = None;
    
    def initialize(self):
        self.axis = int(self.setting['Axis']);
        self.zeropad = self.setting['Zero Pad'] in ['true','True'];
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                
                assert self.axis >= 0 and self.axis < len(self.signalHeaderIn.dimensionSizes)-1, \
                    'Diff cannot operate on the time axis.';
                assert self.signalHeaderIn.dimensionSizes[self.axis] > 1, \
                    'Diff axis must have a dimension size greater than 1.';
                
                if not self.zeropad:
                    self.signalHeaderOut.dimensionSizes[self.axis] -= 1;
                    
                ix = int(np.sum(self.signalHeaderIn.dimensionSizes[:self.axis]));
                for i in range(0,self.signalHeaderIn.dimensionSizes[self.axis]-1):
                    self.signalHeaderOut.dimensionLabels[ix+i] += '-' + \
                        self.signalHeaderIn.dimensionLabels[ix+i+1];
                del self.signalHeaderOut.dimensionLabels[ \
                    ix+self.signalHeaderIn.dimensionSizes[self.axis]-1];
                
                if self.zeropad:
                    self.signalHeaderOut.dimensionLabels.insert(ix,'zeropad');
                
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes);
                data = np.diff(data,axis=self.axis);
                
                if self.zeropad:
                    if self.zeros is None:
                        zerodim = list(data.shape);
                        zerodim[self.axis] = 1;
                        self.zeros = np.zeros(zerodim);
                    data = np.concatenate((self.zeros,data),axis=self.axis);
                    
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, data.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVDiff();