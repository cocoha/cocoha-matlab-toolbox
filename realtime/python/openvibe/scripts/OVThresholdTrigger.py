# -*- coding: utf-8 -*-
"""
OVThresholdTrigger is an OpenViBE plugin that sends a trigger whenever a specified channel in the
input stream exceeeds a specified threshold. Once a trigger is fired, a delay can be specified to
prevent subsequent triggers from being fired for a certain period of time.

The OpenViBE I/O types are:
Input               = Signal
Output              = Stimulations

The following settings need to be set in OpenViBE:
'Stimulation'       = (String) OpenViBE stimulation label.
'Channel Name'      = (String) Name of channel to threshold.
'Threshold'         = (Float) Threshold level.
'Threshold Type'    = (String)
                      "pos" - don't invert channel polarity.
                      "neg" - invert channel polarity.
                      "abs" - use absolute value of channel.
'Reset Delay'       = (Float) Minimum time between triggers.

Created on Mon Nov  2 12:47:11 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy;

class OVThresholdTrigger(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.stimLabel = None;
        self.stimCode = None;
        self.channelName = None;
        self.threshold = None;
        self.threshType = None;
        self.resetDelay = None;
        self.triggerT = float("-Inf");  # Last time trigger was sent
        
        self.signalHeader = None;
        self.channelIx = None;
        self.lastTime = 0;
    
    def initialize(self):
        self.stimLabel = self.setting['Stimulation'];
        self.stimCode = OpenViBE_stimulation[self.stimLabel];
        self.output[0].append(OVStimulationHeader(0., 0.));
        
        self.channelName = self.setting['Channel Name'];
        self.threshold = float(self.setting['Threshold']);
        self.threshType = self.setting['Threshold Type'];
        self.resetDelay = float(self.setting['Reset Delay']);
    
    def process(self):
        # Declare stimset with clock time - update start and end times later based on chunk info
        stimSet = OVStimulationSet(self.getCurrentTime(), self.getCurrentTime()+1./self.getClock());
        minTime = numpy.inf;
        maxTime = -numpy.inf;
        
        try:
            for chunkIndex in range(len(self.input[0]))[::-1]:
                if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                    self.signalHeader = self.input[0].pop();
                    if self.channelIx is None:                          # Only need to set this once
                        self.channelIx = self.signalHeader.dimensionLabels.index(self.channelName);
                elif type(self.input[0][chunkIndex]) == OVSignalBuffer: # Perform thresholding
                    chunk = self.input[0].pop();
                    minTime = min(chunk.startTime, minTime);
                    maxTime = max(chunk.endTime, maxTime);
                    if chunk.endTime - self.triggerT > self.resetDelay \
                            or (self.triggerT < 0 and self.resetDelay == float('Inf')):
                        # Can start thresholding within chunk
                        numpyBuffer = numpy.array(chunk).reshape(tuple(self.signalHeader.dimensionSizes));
                        numpyBuffer = numpyBuffer[self.channelIx,:];
                        
                        ix = max(0,(self.triggerT + self.resetDelay - chunk.startTime)/self.signalHeader.samplingRate);
                        ix = int(ix);
                        
                        # Handle threshold types
                        if self.threshType == 'neg':
                            numpyBuffer = -numpyBuffer;
                        elif self.threshType == 'abs':
                            numpyBuffer = abs(numpyBuffer);
                            
                        # Threshold and append triggers
                        thix = numpy.where(numpyBuffer[ix:] > self.threshold);
                        if len(thix[0]) > 0:
                            self.triggerT = chunk.startTime + float(ix + thix[0][0])/self.signalHeader.samplingRate;
                            stimSet.append(OVStimulation(self.stimCode, self.triggerT, 0.));
                else:
                    self.input[0].pop();    # Still need to pop
                
            if maxTime > -numpy.inf and minTime < numpy.inf:
                stimSet.startTime = minTime;
                stimSet.endTime = maxTime;
                self.output[0].append(stimSet);
            self.lastTime = max(self.lastTime, maxTime);
        except TypeError:
            pass;   # Weird error, but only occurs when there's nothing in the input so we can ignore
    
    def uninitialize(self):
        self.output[0].append(OVStimulationEnd(self.lastTime,self.lastTime));

box = OVThresholdTrigger();