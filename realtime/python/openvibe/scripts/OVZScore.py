# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 21:55:48 2017

@author: dwong
"""

import numpy as np;
import scipy.io as scpio;

class OVZScore(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;
        self.lastTime = 0.;
        self.mean = None;
        self.std = None;
    
    def initialize(self):
        mat = scpio.loadmat(self.setting['Mat File']);
        self.mean = mat['mean'].flatten();
        self.mean = self.mean.reshape((self.mean.size,1));
        self.std = mat['std'].flatten();
        self.std = self.std.reshape((self.std.size,1));
        assert self.mean.size == self.std.size, 'mean and std matricies are not of the same size.';
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop();
                self.output[0].append(self.signalHeader);
                
                assert self.mean.size == self.signalHeader.dimensionSizes[0], \
                    'The number of channels does not match the matrix sizes.';
                
                self.mean = np.tile(self.mean,\
                    (1,self.signalHeader.dimensionSizes[-1]));
                self.std = np.tile(self.std,\
                    (1,self.signalHeader.dimensionSizes[-1]));
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeader.dimensionSizes);
                data = (data-self.mean)/self.std;
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, data.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));

box = OVZScore();