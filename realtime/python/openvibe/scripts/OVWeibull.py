"""
Created on Thu Jul 5 12:00:01 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy as np;

class OVWeibull(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;               # Input signal header
        self.lastTime = 0;
        self.lmbda = 1.;
        self.K = 1.;
    
    def initialize(self):
        self.lmbda = float(self.setting['Lambda']);
        self.K = float(self.setting['K']);


    def process(self):
        for chunkIndex in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                self.signalHeader = self.input[0].pop();
                self.output[0].append(self.signalHeader);
            elif type(self.input[0][chunkIndex]) == OVSignalBuffer: # Perform thresholding
                chunk = self.input[0].pop();
                data = np.array(chunk).flatten() #reshape(tuple(self.signalHeader.dimensionSizes));
                dataOut = 1-np.exp(-np.power(np.abs(data)/self.lmbda,self.K));
                dataOut[data<0] = -dataOut[data<0];
            
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, dataOut.flatten().tolist());
                self.output[0].append(chunk);
                self.lastTime = chunk.endTime;
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));

box = OVWeibull();