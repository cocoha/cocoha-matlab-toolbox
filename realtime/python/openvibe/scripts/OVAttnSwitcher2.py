# -*- coding: utf-8 -*-
"""
OVATTNSWITCHER2 simulates attention switching by swapping 2 channels (e.g. audio envelope
channels). This is similar to OVATTNSWITCHER, except inter-trial periods are ignored so that
switching can be done back-and-forth continuously.

TODO: Permit swapping between 2 sets of channels so spectrograms can be handled.
Created on Fri Sep 14 16:00:25 2018

@author: dwong
"""

import numpy as np

class OVAttnSwitcher2(OVBox):
    def __init__(self):
        OVBox.__init__(self)
        self.trialStartStims = []
        self.trialEndStims = []
        self.switchChannels = []
        self.switchStims = []
        self.interval = 1.
        self.signalHeader = None
        self.trialFlag = -2         # indexes 0-N indicate which trial start stim is active. Index -1 indicates EndStim received. Index -2 indicates EndStim sent.
        self.trialStart = 0.
        self.trialEnd = np.Inf
        self.switchFlag = False
        self.intertrial = 0.        # time spent between trials
        self.lastTime = 0.
    
    def initialize(self):
        self.trialStartStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['Start Stimulations'].split(',')]
        self.trialEndStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['End Stimulations'].split(',')]
        self.switchChannels = [int(i) for i in self.setting['Switch Channels'].split(',')]
        self.switchStims = [OpenViBE_stimulation[i.strip()] for i in self.setting['Switch Stimulations'].split(',')]
        self.interval = float(self.setting['Interval'])
    
    def process(self):
        for chunkIx in range(len(self.input[1])):
            chunk = self.input[1].pop()
            if type(chunk) == OVStimulationSet:
                for stimix in range(len(chunk)):
                    stim = chunk.pop()
                    if stim.identifier in self.trialEndStims:
                        self.trialFlag = -1
                        self.trialEnd = stim.date
                        #print 'End Stim'
                    elif stim.identifier in self.trialStartStims:
                        ix = self.trialStartStims.index(stim.identifier)
                        self.trialFlag = ix
                        self.trialStart = stim.date
                        self.trialEnd = np.Inf
                        self.switchFlag = True
            else:
                self.output[1].append(chunk)
        
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop()
                self.output[0].append(self.signalHeader)
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop()
                self.lastTime = chunk.endTime
                data = np.array(chunk)
                data = data.reshape(self.signalHeader.dimensionSizes)
                
                if self.trialFlag > 0:  # All trial types > 0 will be treated as pre-switched
                    data[self.switchChannels,] = data[self.switchChannels[::-1],]   # Swap channels back
                
                stim = None
                if self.trialFlag >= 0 and chunk.endTime < self.trialEnd:
                    if ((chunk.startTime - self.intertrial) // self.interval) % 2 == 1:
                        data[self.switchChannels,] = data[self.switchChannels[::-1],]   # Swap channels
                        if not self.switchFlag:
                            self.switchFlag = True
                            stim = OVStimulation(self.switchStims[1],chunk.startTime-self.intertrial,0.)
                            #print 'switch1'
                    else:
                        if self.switchFlag:
                            self.switchFlag = False
                            stim = OVStimulation(self.switchStims[0],chunk.startTime-self.intertrial,0.)
                            #print 'switch2'
#                
                    # Send signal
                    chunk = OVSignalBuffer(chunk.startTime-self.intertrial, chunk.endTime-self.intertrial, \
                        data.flatten().tolist())
                    self.output[0].append(chunk)
                else:
                    self.intertrial += chunk.endTime-chunk.startTime

                # Send stimulations
                if self.trialFlag >= 0:
                    stimSet = OVStimulationSet(chunk.startTime-self.intertrial, chunk.endTime-self.intertrial)
                    if stim:
                        stimSet.append(stim)
                    self.output[1].append(stimSet)
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop())
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime))

box = OVAttnSwitcher2()