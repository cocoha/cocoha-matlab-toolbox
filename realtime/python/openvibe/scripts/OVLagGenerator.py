# -*- coding: utf-8 -*-
"""
OVLagGenerator delays the input stream by a specified number of lags. Multiple lags can be
specified and will result in a new 'lag' dimension in the output. This dimension will be the
first dimension.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Lags'                  = (string) comma delimited string of lags. Negative lags will be read in as
                          positive. This is useful for ordering lags from greatest to smallest.

Created on Mon Nov 27 12:36:18 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import numpy as np;
from scipy.signal import lfilter;

class OVLagGenerator(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;
        self.lastTime = 0.;
        
        self.lags = [];
        self.maxLag = 0;
        self.dataBuffer = [];
    
    def initialize(self):
        self.lags = np.abs(parseRange(self.setting['Lags']));
        self.maxLag = np.max(self.lags);
        self.kernels = [];
        self.zi = [];                           # Initial conditions for filter
        for i in range(len(self.lags)):
            self.kernels.append(np.zeros((self.lags[i]+1,)));
            self.kernels[-1][-1] = 1.;
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                # Send signal header, with additional lag dimension if necessary
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                if len(self.lags) > 1:
                    self.signalHeaderOut.dimensionSizes.insert(0,len(self.lags));
                    for i in range(len(self.lags)):
                        self.signalHeaderOut.dimensionLabels.insert(i,'lag'+str(self.lags[i]));
                self.output[0].append(self.signalHeaderOut);
                if len(self.zi) == 0:
                    for i in range(len(self.lags)):
                        self.zi.append([]);
                        for j in range(np.prod(self.signalHeaderIn.dimensionSizes[:-1])):
                            self.zi[i].append(np.zeros((self.lags[i]-1,)));
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                
                data = data.reshape((np.prod(self.signalHeaderIn.dimensionSizes[:-1]), \
                    self.signalHeaderIn.dimensionSizes[-1]));
                dataOut = np.zeros((len(self.kernels), \
                    np.prod(self.signalHeaderIn.dimensionSizes[:-1]), \
                    self.signalHeaderIn.dimensionSizes[-1]));
                
                # Filter data
                for i in range(len(self.lags)):
                    dataOut[i,:,:],self.zi[i] = lfilter(self.kernels[i],[1.],data,zi=self.zi[i]);
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, dataOut.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVLagGenerator();


"""
Parses a string of numbers separated by commas and dashes into a list of integers.
Taken from:
https://stackoverflow.com/questions/6405208/how-to-convert-numeric-string-ranges-to-a-list-in-python
"""
def parseRange(x):
    result = [];
    for part in x.split(','):
        if '-' in part:
            a, b = part.split('-');
            a, b = int(a), int(b);
            result.extend(range(a, b + 1));
        else:
            a = int(part);
            result.append(a);
    return result;
