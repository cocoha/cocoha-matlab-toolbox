# -*- coding: utf-8 -*-
"""
OVSmooth performs left-aligned smoothing on the input stream. Multiple smoothing kernel sizes can
be specified and will result in a new 'smooth' dimension in the output. This dimension will be the
first dimension.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'Smooth'                = (string) comma delimited string of smoothing kernel lengths.

Created on Mon Aug 28 13:34:51 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import numpy as np;
from scipy.signal import lfilter;

class OVSmooth(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.smooth = None;                     # Array of smoothing kernel lengths
        self.maxSmooth = 0;                     # Maximum kernel length
        self.lastTime = 0;                      # End time of last chunk sent (start time of next chunk)
        self.kernels = [];
        self.zi = [];                           # Initial conditions for filter
    
    def initialize(self):
        self.smooth = self.setting['Smooth'].split(',');
        self.smooth = [int(x) for x in self.smooth];
        self.maxSmooth = max(self.smooth);
        for i in range(len(self.smooth)):
            self.kernels.append(np.ones((self.smooth[i],))/self.smooth[i]);
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                # Send signal header, with additional smoothing dimension if necessary
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                if len(self.smooth) > 1:
                    self.signalHeaderOut.dimensionSizes.insert(0,len(self.smooth));
                    for i in range(len(self.smooth)):
                        self.signalHeaderOut.dimensionLabels.insert(i,'smooth'+str(self.smooth[i]));
                self.output[0].append(self.signalHeaderOut);
                if len(self.zi) == 0:
                    for i in range(len(self.smooth)):
                        self.zi.append([]);
                        for j in range(np.prod(self.signalHeaderIn.dimensionSizes[:-1])):
                            self.zi[i].append(np.zeros((self.smooth[i]-1,)));
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                
                data = data.reshape((np.prod(self.signalHeaderIn.dimensionSizes[:-1]), \
                    self.signalHeaderIn.dimensionSizes[-1]));
                dataOut = np.zeros((len(self.kernels), \
                    np.prod(self.signalHeaderIn.dimensionSizes[:-1]), \
                    self.signalHeaderIn.dimensionSizes[-1]));
                
                # Filter data
                for i in range(len(self.smooth)):
                    dataOut[i,:,:],self.zi[i] = lfilter(self.kernels[i],[1.],data,zi=self.zi[i]);
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, dataOut.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
                
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVSmooth();