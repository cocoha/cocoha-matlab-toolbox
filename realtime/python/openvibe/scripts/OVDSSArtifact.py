# -*- coding: utf-8 -*-
"""
OVDSSArtifact removes artifacts using Matlab matrices computed using DSS. These matrices are
obtained when CO_DENOISE is given the cfg.FIELD.eog.realtime field.

The OpenViBE I/O types are:
Input 0             = signal
Output 0            = signal

The following settings need to be set in OpenViBE:
'Mat File'          = (filename) Matlab file containing the todss and r matrices obtained from
                      CO_DENOISE.

Created on Tue Sep 12 17:07:05 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy as np;
import scipy.io as scpio;

class OVDSSArtifact(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;
        self.lastTime = 0.;         # End time of last chunk received
        self.dssmat = None;         # Matrix for computing artifact signal(s) in DSS component space
        self.rmat = None;           # Matrix for removing artifact signal(s) (component x channel)
    
    def initialize(self):
        mat = scpio.loadmat(self.setting['Mat File']);
        self.dssmat = mat['todss'].transpose();
        self.rmat = mat['r'].transpose();
        assert self.dssmat.shape[0]==self.rmat.shape[1] and \
            self.dssmat.shape[1]==self.rmat.shape[0], 'Invalid dssmat / rmat sizes.';
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeader = self.input[0].pop();
                self.output[0].append(self.signalHeader);
                
                assert self.signalHeader.dimensionSizes[0] == self.dssmat.shape[1], \
                    'Number of channels does not match the number of rows in todss matrix.';
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                
                data = np.array(chunk);
                data = data.reshape(self.signalHeader.dimensionSizes);
                art = np.dot(self.dssmat,data);
                data -= np.dot(self.rmat,art);
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, \
                    data.flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));

box = OVDSSArtifact();