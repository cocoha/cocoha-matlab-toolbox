# -*- coding: utf-8 -*-
"""
OVClassifier performs classification on the input signals using a classifier that has been saved to
a Pickle file. The classifier must have a .predict function for class output (which must be
numeric), and/or a .decision_function function for decision function output. See sklearn classifiers
for examples.

The OpenViBE I/O types are:
Input 0             = signal
Output 0            = signal

The following settings need to be set in OpenViBE:
Pickle File         = (filename) the filename of the pickled classifier.
Output Type         = (string) 'class' or 'decision'. Determines whether to output the class or the
                      decision function.

Created on Wed Aug 30 15:42:20 2017

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import cPickle;
import numpy as np;

class OVClassifier(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.lastTime = 0;                      # Time of last sent sample
        self.classifier = None;                 # Classifier instance
        self.decision = False;                  # If True, will output the decision function
    
    def initialize(self):
        with open(self.setting['Pickle File'], 'rb') as f:
            self.classifier = cPickle.load(f);
        if self.setting['Output Type'] == 'decision':
            self.decision = True;
    
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                
                assert len(self.signalHeaderIn.dimensionSizes) == 2, 'Input must be 2D.';
                
                if self.decision:
                    X = np.zeros((1,self.signalHeaderIn.dimensionSizes[0]));
                    y = self.classifier.decision_function(X).flatten();
                    self.signalHeaderOut.dimensionSizes[0] = len(y);
                    self.signalHeaderOut.dimensionLabels = ['decisionfcn']*len(y);
                else:
                    X = np.zeros((1,self.signalHeaderIn.dimensionSizes[0]));
                    y = self.classifier.predict_proba(X).flatten();
                    self.signalHeaderOut.dimensionSizes[0] = len(y);
                    self.signalHeaderOut.dimensionLabels = ['class']*len(y);
                    
                self.signalHeaderOut.dimensionLabels += \
                    self.signalHeaderIn.dimensionLabels[-self.signalHeaderOut.dimensionSizes[-1]:];
                self.output[0].append(self.signalHeaderOut);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                chunk = self.input[0].pop();
                self.lastTime = chunk.endTime;
                data = np.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes).transpose();
                
                if self.decision:   # SVM
                    dataOut = self.classifier.decision_function(data);
                else:
                    dataOut = self.classifier.predict_proba(data);
                
                chunk = OVSignalBuffer(chunk.startTime, chunk.endTime, \
                    dataOut.transpose().flatten().tolist());
                self.output[0].append(chunk);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVClassifier();