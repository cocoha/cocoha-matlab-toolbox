# -*- coding: utf-8 -*-
"""
OVAUDIOBALANCE is an OpenViBE OVBox that takes an input signal with values between -1 and 1, and
uses it to control the balance on a 2-channel WAV file. If an accompanying MPEG-1 video file is
specified, both audio and video will play at the same time (any sound in the video will not be
played). Bars will be displayed on the left and right side of the video, indicating the volume of
the left and right channel.

Input configuration: one input of type 'Signal'.
Output configuration: no outputs.

Settings:
'Channel Name'      - (String) the name of the input channel with the control signal.
'Minimum Volume'    - (Float) the minimum volume allowed for each channel.
'Audio File'        - (Filename) the location of a stereo WAV file.
'Video File'        - (Filename, optional) the location of an MPEG-1 video file.
'Video Scale'       - (Float, optional) the scale factor for the video.

Created on Mon May  2 17:27:18 2016

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH
@author: Daniel D.E. Wong
"""

import pygame;
import numpy;

BAR_WIDTH = 0.1;    # Width of each bar relative to width of video

class OVAudioBalance(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.channelName = None;                # Name of input channel
        self.channelIx = None;                  # Index of input channel
        self.signalHeaderIn = None;             # Input signal header
        self.minVol = 0;                        # Minimum volume
        self.audio = None;                      # Audio instance
        self.audioChan = 0;
        self.video = None;                      # Optional video
        self.videoScale = 1.;
        self.screen = None;
        self.pygameRunning = False;             # Whether pygame is running
    
    def initialize(self):
        pygame.init();
        
        self.channelName = self.setting['Channel Name'];
        
        self.minVol = float(self.setting['Minimum Volume']);
        
        audioFile = self.setting['Audio File'];
        self.audio = pygame.mixer.Sound(audioFile);
        
        if 'Video File' in self.setting:
            videoFile = self.setting['Video File'];
            
            if 'Video Scale' in self.setting:
                self.videoScale = float(self.setting['Video Scale']);
            
            if len(videoFile) > 0:
                self.video = pygame.movie.Movie(videoFile);
                w, h = self.video.get_size();
                w = int(w * self.videoScale);
                h = int(h * self.videoScale);
                self.screen = pygame.display.set_mode((int(round(w*(1+BAR_WIDTH*2))),h));
                x_offset = int(round(w*BAR_WIDTH));
                self.video.set_display(self.screen, pygame.Rect((x_offset,0), (w,h)));
                self.video.set_volume(0);
                self.video.play();
        
        self.audioChan = self.audio.play();
        
        pygame.event.set_allowed((pygame.QUIT));
        
        self.pygameRunning = True;
    
    def process(self):
        # Handle quit event
        evt = pygame.event.get();
        for evt_it in evt:
            if evt_it.type == pygame.QUIT:
                self.audio.stop();
                if not self.video is None:
                    self.video.stop();
                self.pygameRunning = False;
            
        # Rewind audio and video if necessary
        if self.pygameRunning:
            videoRunning = False;
            if not self.video is None:
                videoRunning = self.video.get_busy();
            if not (self.audioChan.get_busy() or videoRunning):
                if not self.video is None:
                    self.video.rewind();
                    self.video.play();
                self.audioChan = self.audio.play();

        try:
            for chunkIndex in range(len(self.input[0]))[::-1]:
                if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                    self.signalHeader = self.input[0].pop();
                    if self.channelIx is None:                          # Only need to set this once
                        self.channelIx = self.signalHeader.dimensionLabels.index[self.channelName];
                elif type(self.input[0][chunkIndex]) == OVSignalBuffer: # Get gain from chunk mean
                    chunk = self.input[0].pop();
                    
                    if self.pygameRunning:
                        numpyBuffer = numpy.array(chunk).reshape( \
                            tuple(self.signalHeader.dimensionSizes));
                        numpyBuffer = numpyBuffer[self.channelIx,:];
                        balance = max(numpy.mean(numpyBuffer),-1);
                        balance = min(balance,1);
                        
                        volL = self.minVol + (1-balance)/2*(1-self.minVol);
                        volR = self.minVol + (1+balance)/2*(1-self.minVol);
                        self.audioChan.set_volume(volL,volR);
                        
                        # Draw volume level bars
                        if not self.video is None:
                            w, h = self.video.get_size();
                            w = int(w * self.videoScale);
                            h = int(h * self.videoScale);
                            rectL = pygame.Rect((0, 0), \
                                (int(round(w*BAR_WIDTH))-1, int(round(h*volL))));
                            rectL_upd = pygame.Rect((0, 0), (int(round(w*BAR_WIDTH))-1, h));
                            rectR = pygame.Rect((int(round((1+BAR_WIDTH)*w)), 0), \
                                (int(round(w*BAR_WIDTH)), int(round(h*volR))));
                            rectR_upd = pygame.Rect((int(round((1+BAR_WIDTH)*w)), 0), \
                                (int(round(w*BAR_WIDTH)), h));
                            pygame.draw.rect(self.screen, (0,0,0), rectL_upd);
                            pygame.draw.rect(self.screen, (0,0,255), rectL);
                            pygame.draw.rect(self.screen, (0,0,0), rectR_upd);
                            pygame.draw.rect(self.screen, (255,0,0), rectR);
                            pygame.display.update([rectL_upd, rectR_upd])
                else:
                    self.output[0].append(self.input[0].pop());
        except TypeError:
            pass;   # Weird error, but only occurs when there's nothing in the input so we can ignore
    
    def uninitialize(self):
        if self.pygameRunning:
            if not self.video is None:
                self.video.stop();
            self.audio.stop();
        pygame.quit();

box = OVAudioBalance();