# -*- coding: utf-8 -*-
"""
OVResample resamples the input stream to a new sampling rate using a 1-D interpolation scheme
supported by SciPy. If downsampling, the user should perform a lowpass antialiasing
filter beforehand. This box is designed to be less restrictive than the native implementation.

The OpenViBE I/O types are:
Input 0                 = signal
Output 0                = signal

The following settings need to be set in OpenViBE:
'New Sampling Frequency'= (float) rate to which input data should be resampled
'Samples Per Chunk'     = (integer) number of time samples per chunk. If set to <= 0, this parameter
                          will be taken from the input stream.
'Interpolation Type'    = (string) 'linear'/'nearest'/'zero'/'slinear'/'quadratic'/'cubic' an
                          interpolation scheme supported by scipy.interpolation.interp1d. Note that
                          a <= 1st order interpolation scheme is recommended as higher orders
                          require significantly more computational resources.

Created on Tue Nov 24 22:25:35 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import copy;
import math;
import numpy;
from scipy import interpolate;

class OVResample(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeaderIn = None;             # Input signal header
        self.signalHeaderOut = None;            # Output signal header at new sampling rate and chunk size
        self.newfs = None;                      # New sampling rate (read from settings)
        self.samplesPerChunk = None;            # New chunk size (read from settings)
        self.interpOrder = None;                # Interpolation order (dependent on specified interpolation scheme)
        self.dataBuffer = numpy.array([]);
        self.startTimes = numpy.array([]);      # Start times of samples in data buffer
        self.lastTime = 0;                      # End time of last chunk sent (start time of next chunk)
        self.counter = 0;
    
    def initialize(self):
        # Read in OpenViBE box settings
        self.newfs = float(self.setting['New Sampling Frequency']);
        self.samplesPerChunk = int(self.setting['Samples Per Chunk']);
        self.interpType = self.setting['Interpolation Type'];
        if self.interpType == 'linear':
            self.interpOrder = 1;
        elif self.interpType == 'nearest':
            self.interpOrder = 0;
        elif self.interpType == 'zero':
            self.interpOrder = 0;
        elif self.interpType == 'slinear':
            self.interpOrder = 1;
        elif self.interpType == 'quadratic':
            self.interpOrder = 2;
        elif self.interpType == 'cubic':
            self.interpOrder = 3;
        else:
            raise RuntimeError('Unsupported interpolation type');
        
    def process(self):
        for chunkIx in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIx]) == OVSignalHeader:
                # Send signal header with new sampling rate and chunk size
                self.signalHeaderIn = self.input[0].pop();
                self.signalHeaderOut = copy.deepcopy(self.signalHeaderIn);
                self.signalHeaderOut.samplingRate = self.newfs;
                if self.samplesPerChunk > 0:    # Modify output samples per chunk if settings value > 0
                    self.signalHeaderOut.dimensionSizes[1] = self.samplesPerChunk;
                self.output[0].append(self.signalHeaderOut);
                
                # Pad data buffer with zeros (before t=0) for interpolation
                data = numpy.zeros([self.signalHeaderIn.dimensionSizes[0], int(math.ceil((self.interpOrder+1)/2.))]);
                t = numpy.arange(-math.ceil((self.interpOrder+1)/2.)/self.signalHeaderIn.samplingRate, 0, \
                        1./self.signalHeaderIn.samplingRate);
                self.dataBufferAppend(data);
                self.startTimes = numpy.append(self.startTimes,t);
            elif type(self.input[0][chunkIx]) == OVSignalBuffer:
                # Append incoming data to buffer
                chunk = self.input[0].pop();
                data = numpy.array(chunk);
                data = data.reshape(self.signalHeaderIn.dimensionSizes);
                self.dataBufferAppend(data);
                
                # Beginning time of each sample
                t = numpy.arange(chunk.startTime, chunk.endTime, \
                        (chunk.endTime-chunk.startTime)/self.signalHeaderIn.dimensionSizes[1]);
                self.startTimes = numpy.append(self.startTimes,t);
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop());
        
        while self.signalHeaderOut is not None:
            # Determine if there are enough samples in the data buffer to interpolate next sent chunk
            tEnd = self.lastTime + (self.signalHeaderOut.dimensionSizes[1]) / \
                    self.signalHeaderOut.samplingRate;          # End time of last sample in chunk
            if len(self.startTimes) < self.interpOrder or self.startTimes[-int(math.ceil((self.interpOrder+1)/2.))] <= tEnd:
                break;
            
            # Interpolate samples at new rate
            sampleIx = numpy.arange(self.signalHeaderOut.dimensionSizes[1]);
            t = self.lastTime + sampleIx/self.signalHeaderOut.samplingRate; # Sample times to interpolate
            f = interpolate.interp1d(x=self.startTimes, y=self.dataBuffer, kind=self.interpType, copy=False, assume_sorted=True);
            data = f(t);
            chunk = OVSignalBuffer(self.lastTime, tEnd, data.flatten().tolist());
            self.output[0].append(chunk);
            self.lastTime = tEnd;
        
        # Trim buffers
        if self.signalHeaderOut is not None:
            ix = numpy.where(self.startTimes > self.lastTime-math.ceil((self.interpOrder+1)/2.))[0];
            self.startTimes = self.startTimes[ix];
            self.dataBuffer = self.dataBuffer[:,ix];
        
    # Manages appending of data to buffer (handles case where buffer is empty)
    def dataBufferAppend(self, x):
        if len(self.dataBuffer) == 0:
            self.dataBuffer = x;
        else:
            self.dataBuffer = numpy.concatenate((self.dataBuffer,x),axis=1);
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime, self.lastTime));
        
box = OVResample();