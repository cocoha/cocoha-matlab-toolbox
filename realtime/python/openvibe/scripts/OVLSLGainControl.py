# -*- coding: utf-8 -*-
"""
OVLSLGAINCONTROL is an OVBox is used to convert an OpenViBE signal into an LabStreamingLayer signal
that can be used to control AUDIOIN2LSL or WAV2LSL. This signal takes the form of a diagonal mixing
matrix, where the diagonal elements are controlled by OVLSLGAINCONTROL.

The OpenViBE I/O types are:
Input 0                 = signal

Settings:
'Channel Map'       = (string) mapping between input and output channels represented as a comma-
                      separated list the same length as the number of output channels.
'LSL Name'          - (String) name of output LSL stream.
'LSL ID'            - (String) ID of output LSL stream.

LabStreamingLayer Setup Notes:
1) Obtain LabStreamingLayer dependencies by running get_deps.py in the labstreaminglayer base
   directory and premangle_boost.py in the labstreaminglayer/LSL/liblsl/external/ directory.
2) Install PyLSL as a Python module by running 'setup.py build' and 'setup.py install' in the
   /labstreaminglayer/LSL/liblsl-Python/ directory.

Created on Wed Dec  9 15:02:10 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH
@author: Daniel D.E. Wong
"""

import numpy as np;
from pylsl import StreamInfo, StreamOutlet;

class OVLSLGainControl(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        self.signalHeader = None;
        self.lastTime = 0.;
        
        self.lslInfo = None;
        self.lslStream = None;
        
        self.channelMap = [];
    
    def initialize(self):
        self.channelMap = [int(x) for x in self.setting['Channel Map'].split(',')];
        self.lslName = self.setting['LSL Name'];
        self.lslId = self.setting['LSL ID'];
        
        # Create LSL stream
        self.lslInfo = StreamInfo(name=self.lslName, channel_count=len(self.channelMap)**2, \
            channel_format='float32', source_id=self.lslId);
        self.lslOutlet = StreamOutlet(self.lslInfo);
    
    def process(self):
        try:
            for chunkIndex in range(len(self.input[0]))[::-1]:
                if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                    self.signalHeader = self.input[0].pop();
                    assert max(self.channelMap) < self.signalHeader.dimensionSizes[0], \
                        'Channel Map references a nonexistent input channel.';
                elif type(self.input[0][chunkIndex]) == OVSignalBuffer: # Get gain from chunk mean
                    chunk = self.input[0].pop();
                    self.lastTime = chunk.endTime;
                    npBuffer = np.array(chunk).reshape(tuple(self.signalHeader.dimensionSizes));
                    
                    ctrlMat = np.eye(len(self.channelMap));
                    for i in range(len(self.channelMap)):
                        if self.channelMap[i] >= 0:
                            ctrlMat[i,i] = np.mean(npBuffer[self.channelMap[i],:]);
                    self.lslOutlet.push_sample(ctrlMat.flatten().tolist());
                else:
                    self.output[0].append(self.input[0].pop());
                    
        except TypeError:
            pass;   # Weird error, but only occurs when there's nothing in the input so we can ignore
            
    def uninitialize(self):
        del self.lslOutlet;

box = OVLSLGainControl();