# -*- coding: utf-8 -*-
"""
OVSignalMerger is designed to merge multiple signal streams with the same sampling rate. Unlike the
native version, this box is designed to be robust to different start/end times between the two
streams. This is handled by aligning the start time of all streams to the start time of the last
arriving stream within the streams' sampling rate. This start time is based on the first arriving
chunk, and not the header. If all streams start at the same time, all streams will be aligned
to the timing of the first stream (index 0).

Any number of input signals and only one output signal can be specified. The OpenViBE box does not
require any configuration settings.

Created on Mon Nov 16 16:06:35 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH/INI
@author: Daniel D.E. Wong
"""

import numpy;

class OVSignalMerger(OVBox):
    def __init__(self):
        OVBox.__init__(self);
        
        self.signalHeaderInitFlag = False;      # If all input signal headers have been received
        self.signalHeaderIn = [];               # Headers of incoming streams
        self.signalHeaderOut = None;            # Header of merged outgoing stream
        
        self.dataBuffer = [];
        self.startTime = [];                    # Start times of first chunk in streams
        self.endTime = [];                      # End times of first chunk in streams
        self.lateIx = None;                     # Index of late stream
        self.bufferTrimFlag = False;            # If buffers have been trimmed to account for start offsets
        self.earlySamples = [];                 # Number of early samples in each stream (to be trimmed from start of buffer)
        self.startTimeOutBuffer = [];           # Buffer of chunk start times to send
        self.endTimeOutBuffer = [];             # Buffer of chunk end times to send
        self.lastTime = 0;                      # End time of last sent chunk (used for uninitialize)
    
    def initialize(self):
        for inx in range(len(self.input)):
            self.signalHeaderIn.append(None);
            self.dataBuffer.append(numpy.array([]));
            self.earlySamples.append(0);
            self.startTime.append(None);
            self.endTime.append(None);
    
    def process(self):
        for inIx in range(len(self.input)):
            
            try:
                for chunkIx in range(len(self.input[inIx]))[::-1]:
                    if type(self.input[inIx][chunkIx]) == OVSignalHeader:
                        self.signalHeaderIn[inIx] = self.input[inIx].pop();
                    elif type(self.input[inIx][chunkIx]) == OVSignalBuffer:
                        # Place data in buffer
                        chunk = self.input[inIx].pop();
                        data = numpy.array(chunk);
                        data = data.reshape(self.signalHeaderIn[inIx].dimensionSizes);
                        self.dataBufferAppend(inIx,data);
                        
                        # Record start time of first arriving chunk for current stream
                        if self.startTime[inIx] is None:
                            self.startTime[inIx] = chunk.startTime;
                            self.endTime[inIx] = chunk.endTime;
                        
                        if inIx == self.lateIx:
                            self.startTimeOutBuffer.append(chunk.startTime);
                            self.endTimeOutBuffer.append(chunk.endTime);
                    else:
                        # End of stream
                        eos = self.input[inIx].pop();
                        eos.startTime = self.lastTime;
                        eos.endTime = self.lastTime;
                        self.output[0].append(eos);
            except TypeError:
                pass;   # Weird error, but only occurs when there's nothing in the input so we can ignore
        
        # Check if signal headers have been received
        if not self.signalHeaderInitFlag:
            self.signalHeaderInitFlag = len([i for i in self.signalHeaderIn if i is None]) == 0;
            
            # Ensure all streams have the same sampling rate once all headers received
            if self.signalHeaderInitFlag:
                fsample = self.signalHeaderIn[0].samplingRate;
                for i in range(1,len(self.signalHeaderIn)):
                    assert self.signalHeaderIn[i].samplingRate == fsample, \
                            "Sampling rate mismatch between streams 0 and {}".format(i);
                
                # Create header for merged signal and send to output
                chanCount = 0;
                sampleCount = 0;
                dimensionLabels = [];
                maxStartTime = float('-Inf');       # Start time to be based on latest start time
                for i in range(len(self.signalHeaderIn)):
                    chanCount += self.signalHeaderIn[i].dimensionSizes[0];
                    sampleCount = max(sampleCount, self.signalHeaderIn[i].dimensionSizes[-1])
                    dimensionLabels.extend(self.signalHeaderIn[i].dimensionLabels[:self.signalHeaderIn[i].dimensionSizes[0]]);
                    maxStartTime = max(maxStartTime, self.signalHeaderIn[i].startTime);
                dimensionSizes = [chanCount, sampleCount];
                dimensionLabels.extend(['']*sampleCount);
                self.signalHeaderOut = OVSignalHeader(maxStartTime,maxStartTime, \
                        dimensionSizes,dimensionLabels,float(fsample));
                self.output[0].append(self.signalHeaderOut);
        
        # Determine which stream starts the latest once data received from all streams
        if self.lateIx is None and len([i for i in self.startTime if i is None]) == 0:
            self.lateIx = self.startTime.index(max(self.startTime));
            self.startTimeOutBuffer.append(self.startTime[self.lateIx]);
            self.endTimeOutBuffer.append(self.endTime[self.lateIx]);
            
            for inIx in range(len(self.input)):
                if inIx != self.lateIx:
                    self.earlySamples[inIx] = round(self.signalHeaderIn[inIx].samplingRate * \
                            (self.startTime[self.lateIx] - self.startTime[inIx]));
            
        # Trim buffers to remove samples from early streams for stream alignment
        if self.lateIx is not None and not self.bufferTrimFlag:
            # Check if there are enough samples in all data buffers
            sufficientSamples = True;
            for inIx in range(len(self.input)):
                if self.dataBuffer[inIx].shape[1] <= self.earlySamples:
                    sufficientSamples = False;
            
            if sufficientSamples:
                # Trim buffer
                for inIx in range(len(self.dataBuffer)):
                    data = data[:,self.earlySamples:];
                self.bufferTrimFlag = True;
            
        # Handle sending of data
        if self.signalHeaderInitFlag:
            outBufferLen = self.signalHeaderOut.dimensionSizes[-1];     # Required output time samples
            minBufferLen = float('Inf');                                # Minimum available time samples
            for data in self.dataBuffer:
                if data.ndim > 1:
                    minBufferLen = min(minBufferLen, data.shape[-1]);
                else:
                    minBufferLen = 0;
            nOutChunks = min(minBufferLen/outBufferLen, len(self.startTimeOutBuffer));  # Number of chunks to send
            
            for i in range(nOutChunks):
                # Construct output chunk from all streams
                data = numpy.zeros(self.signalHeaderOut.dimensionSizes);
                chanix = 0;
                for j in range(len(self.dataBuffer)):
                    nchan = self.dataBuffer[j].shape[0];
                    data[chanix:chanix+nchan,:] = self.dataBuffer[j][:,i*outBufferLen:(i+1)*outBufferLen];
                    chanix += nchan;
                    
                chunk = OVSignalBuffer(self.startTimeOutBuffer[i], self.endTimeOutBuffer[i], data.flatten().tolist());
                self.output[0].append(chunk);
                self.lastTime = self.endTimeOutBuffer[i];
            
            # Remove sent data from buffers
            self.startTimeOutBuffer = self.startTimeOutBuffer[nOutChunks:];
            self.endTimeOutBuffer = self.endTimeOutBuffer[nOutChunks:];
            if nOutChunks > 0:
                for i in range(len(self.dataBuffer)):
                    self.dataBuffer[i] = self.dataBuffer[i][:,nOutChunks*outBufferLen:];
                   
    # Manages appending of data to buffer (handles case where buffer is empty)
    def dataBufferAppend(self, inIndex, x):
        if len(self.dataBuffer[inIndex]) == 0:
            self.dataBuffer[inIndex] = x;
        else:
            self.dataBuffer[inIndex] = numpy.concatenate((self.dataBuffer[inIndex],x),axis=-1);
    
    def uninitialize(self):
        self.output[0].append(OVSignalEnd(self.lastTime,self.lastTime));

box = OVSignalMerger();