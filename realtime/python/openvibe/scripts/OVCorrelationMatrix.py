# -*- coding: utf-8 -*-
"""
Created on Sun Oct  7 16:55:07 2018

@author: dwong
"""

import numpy as np


class CorrelationMatrix(OVBox):
    
    def __init__(self):
        OVBox.__init__(self)
        self.fsIn = None
        self.fsOut = None
        self.dimIn = None
        self.dimOut = None
        self.lmb = 0.1
        
        self.C = None
        self.buffer = None
    
    def initialize(self):
        self.lmb = float(self.setting("Lambda"))
    
    def process(self):
        for chunkIndex in range(len(self.input[0]))[::-1]:
            if type(self.input[0][chunkIndex]) == OVSignalHeader:   # Signal header - get channel ix
                signalHeaderIn = self.input[0].pop()
                self.fsIn = self.signalHeaderIn.samplingRate
                self.dimIn = self.signalHeaderIn.dimensionSizes
                self.dimOut = [self.dimIn[0], self.dimIn[0], self.dimIn[1]]
                dimLabelOut = ['']*(self.dimIn[0]**2 + self.epochLen)
                smHeaderOut = OVStreamedMatrixHeader(signalHeaderIn.startTime, \
                    signalHeaderIn.endTime, self.dimOut, dimLabelOut)
                self.output[0].append(smHeaderOut)
                
                self.C = np.zeros(self.dimOut[:-1])
            elif type(self.input[0][chunkIndex]) == OVSignalBuffer:
                chunk = self.input[0].pop()
                data = np.array(chunk).reshape(self.dimIn)
                dataOut = np.zeros(self.dimOut)
                for i in range(data.shape[-1]):
                    self.C = np.dot(data[:,i],data[:,i].T) + self.C*self.lmb/self.fsIn
                    dataOut[:,:,i] = self.C
                chunk = OVStreamedMatrixBuffer(chunk.startTime, chunk.endTime, \
                    dataOut.flatten().tolist())
                self.output[0].append(chunk)
            else:
                # OVSignalEnd - forward to output
                self.output[0].append(self.input[0].pop())