#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
AUDIOIN2LSL streams data from the microphone to LabStreamingLayer. The buffer size controls how
often data is read from the audio buffer and sent over LSL, at the expense of processing overhead.

A control signal can be provided, via LabStreamingLayer in the form of a mixing matrix, with the
number of rows and columns equal to the number of microphone channels. The sampling rate of this
stream can be irregular. The mixing matrix should be sent in the form of a flattened Numpy array,
or an equivalent data structure.

LabStreamingLayer Setup Notes:
1) Obtain LabStreamingLayer dependencies by running get_deps.py in the labstreaminglayer base
   directory and premangle_boost.py in the labstreaminglayer/LSL/liblsl/external/ directory.
2) Install PyLSL as a Python module by running 'setup.py build' and 'setup.py install' in the
   /labstreaminglayer/LSL/liblsl-Python/ directory.


Created on Wed Oct 14 14:37:51 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH
@author: Daniel D.E. Wong
"""

import getopt;
import numpy;
import os;
import pyaudio;
import sys;
import time;
from pylsl import StreamInfo, StreamOutlet, StreamInlet, resolve_byprop;


# Defaults
TIMEOUT         = 0.1;      # Timeout / sleep time
FSAMPLE         = 11025;    # Sample rate for input and output
CHANNELS        = 2;        # Number of microphone channels
PA_FORMAT       = pyaudio.paFloat32;
LSL_FORMAT      = 'float32';
NP_FORMAT       = numpy.float32;
BUFFER          = 256;      # Size of recording buffer
FORWARD_AUDIO   = True;     # Forward audio from mic to sound card output
NAME            = 'audioin2lsl';
SOURCE_ID       = '';
MIX_AUDIO       = True;     # Mix sound card output
MIX_LSL         = False;    # Mix LSL output


def main(argv):
    
    # Set defaults
    fsample = FSAMPLE;
    channels = CHANNELS;
    bufferSz = BUFFER;
    forwardAudio = FORWARD_AUDIO;
    name = NAME;
    source_id = SOURCE_ID;
    ctrlName = None;
    ctrl_id = None;
    mixAudio = MIX_AUDIO;
    mixLSL = MIX_LSL;
    
    # Parse inputs
    try:
        opts, args = getopt.getopt(argv[1:], 'h', 
                                   ['fsample=', 'channels=', 'buffer=', 'forward=','name=', 'id=', \
                                   'ctrlname=', 'ctrlid=', 'mixaudio=', 'mixlsl=']);
    except getopt.GetoptError:
        print 'Error: invalid option(s). Show help using: ' + argv[0] + ' -h';
        sys.exit(2);

    for opt, arg in opts:
        if opt == '-h':
            print argv[0] + ' [options]';
            print '\t--fsample   [' + str(fsample) + '] microphone sampling rate.';
            print '\t--channels  [' + str(channels) + '] number of microphone channels.';
            print '\t--buffer    [' + str(bufferSz) + '] frame size of recording buffer.';
            print '\t--forward   [' + str(forwardAudio) + '] forward audio to sound card output.';
            print '\t--name      [' + str(name) + '] name of outgoing LabStreamingLayer stream.'
            print '\t--id        [' + source_id + '] source ID of outgoing LabStreamingLayer stream.';
            print '\t--ctrlname  name of control LabStreamingLayer stream (inactive by default).';
            print '\t--ctrlid    ID of control LabStreamingLayer stream.';
            print '\t--mixaudio  [' + str(mixAudio) + '] mix sound card output.';
            print '\t--mixlsl    [' + str(mixLSL) + '] mix LSL output.';
            return;
        elif opt == '--fsample':
            fsample = int(arg);
        elif opt == '--channels':
            channels = int(arg);
        elif opt == '--buffer':
            bufferSz = int(arg);
        elif opt == '--forward':
            if arg.lower() == 'true':
                forwardAudio = True;
            else:
                forwardAudio = False;
        elif opt == '--name':
            name = arg;
        elif opt == '--id':
            source_id = arg;
        elif opt == '--ctrlname':
            ctrlName = arg;
        elif opt == '--ctrl_id':
            ctrl_id = arg;
        elif opt == '--mixaudio':
            if arg.lower() == 'true':
                mixAudio = True;
            else:
                mixAudio = False;
        elif opt == '--mixlsl':
            if arg.lower() == 'true':
                mixLSL = True;
            else:
                mixLSL = False;
    
    # Print runtime configuration
    print '[*] Microphone sampling rate: %f' % fsample;
    print '[*] Number of microphone channels: %d' % channels;
    print '[*] Frame size of recording buffer: %d' % bufferSz;
    print '[*] Forward audio to sound card: %s' % str(forwardAudio);
    print '[*] LSL stream name: audioin2lsl';
    print '[*] LSL stream ID: %s' % source_id;
    
    # Default mixing matrix
    mixingMat = numpy.eye(channels,dtype=NP_FORMAT);
    
    # Start LSL output stream
    info = StreamInfo(name,'Audio',channels,float(fsample),LSL_FORMAT,source_id);
    channelInfo = info.desc().append_child('channels');
    for i in range(channels):
        channelInfo.append_child('channel') \
            .append_child_value('label', 'Mic' + str(i)) \
            .append_child_value('type', 'Audio');
    outlet = StreamOutlet(info);
    
    def callback(in_data, frame_count, time_info, status):
        data = numpy.fromstring(in_data, dtype=NP_FORMAT);
        data = data.reshape(frame_count,channels);
        dataMix = numpy.dot(data,mixingMat);
        
        if mixLSL:
            outlet.push_chunk(x=dataMix.tolist(),timestamp=time_info['input_buffer_adc_time']);
        else:
            outlet.push_chunk(x=data.tolist(),timestamp=time_info['input_buffer_adc_time']);
        
        if mixAudio:
            return (dataMix, pyaudio.paContinue);
        else:
            return (data, pyaudio.paContinue);
             
    # Start microphone
    p = pyaudio.PyAudio();
    stream = p.open(format=PA_FORMAT,channels=channels,rate=fsample,input=True,
                    frames_per_buffer=bufferSz,output=forwardAudio,stream_callback=callback);
    
    stream.start_stream();
    
    inInfo = None;  # Stream information
    inlet = None;   # StreamInlet
    try:
        print 'Streaming data (Press Ctrl-C to stop)...';
        while stream.is_active():
            # Scan for input stream
            if ctrlName is not None and inlet is None:
                streams = resolve_byprop('name',ctrlName,timeout=TIMEOUT);
                if len(streams) > 0:
                    if ctrl_id is not None:     # Scan for input stream IDs
                        inInfo = [i for i in streams if i.source_id() == ctrl_id];
                        if len(inInfo) == 0:
                            continue;
                        else:
                            inInfo = inInfo[0];
                    else:
                        inInfo = streams[0];
                        
                    assert inInfo.channel_count() == mixingMat.size, \
                            '{} control channels expected'.format(mixingMat.size);
                    inlet = StreamInlet(inInfo);
                    print '\t[*] Control stream detected.'
            
            # Check input stream for samples
            if inlet is not None:
                chunk,_ = inlet.pull_chunk(timeout=TIMEOUT);
                if len(chunk) >= 1:
                    mixingMat = numpy.array(chunk[-1]).astype(NP_FORMAT);
                    mixingMat = mixingMat.reshape([channels,channels]);
            else:
                time.sleep(TIMEOUT);
    except KeyboardInterrupt:
        print ('\nStopping');
        stream.stop_stream();
        stream.close();
        p.terminate();
    return;
    

if __name__ == "__main__":
    main(sys.argv);