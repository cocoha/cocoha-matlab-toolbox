#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
WAV2LSL plays audio from one or more WAV files and at the same time pipes the data over
LabStreamingLayer. The buffer size controls how often data is read from the audio buffer and sent
over LSL, at the expense of processing overhead. It is recommended to use an 8kHz WAV file when
using this in conjunction with OpenViBE.

LabStreamingLayer Setup Notes:
1) Obtain LabStreamingLayer dependencies by running get_deps.py in the labstreaminglayer base
   directory and premangle_boost.py in the labstreaminglayer/LSL/liblsl/external/ directory.
2) Install PyLSL as a Python module by running 'setup.py build' and 'setup.py install' in the
   /labstreaminglayer/LSL/liblsl-Python/ directory.
   
Created on Fri Oct 30 19:06:23 2015

Copyright 2015, H2020 COCOHA Project, ENS/CNRS, DTU, Oticon, UCL, UZH
@author: Daniel D.E. Wong
"""

import ctypes;
import getopt;
import numpy;
import os
if os.name == 'nt':
    import _winreg as winreg
import pyaudio;
import sys;
import thread;
#import tty;
#import termios;
import time;
import wave;
from pylsl import LostError, StreamInfo, StreamOutlet, StreamInlet, resolve_byprop;

FREE_TRACK_LIB_FILE_NAME = "FreeTrackClient.dll"

# Defaults
TIMEOUT         = 0.1;      # Timeout / sleep time
NREPS           = -1;       # Number of times to play wav file
START_TIME      = 0.;       # Grace period before wav starts playing (zeros will be sent)
PA_FORMAT       = pyaudio.paFloat32;
LSL_FORMAT      = 'float32';
NP_FORMAT       = numpy.float32;
BUFFER          = 256;      # Size of recording buffer
TRIGGER_LEN     = 1;        # Duration of trigger pulse
TRIGGER_CHAN    = -1;
NAME            = 'wav2lsl';
SOURCE_ID       = '';
CTRL_TRIG       = -1;
MIX_AUDIO       = True;     # Mix sound card output
MIX_LSL         = False;    # Mix LSL output
FACETRACK       = False;

sendTrigger     = 0;        # Send a trigger pulse if 1, exit if 2
dirtyTrigger    = False;    # First trigger has been sent
pauseFlag       = False;
zeros           = None;     # Zero-padding for extended trigger channels


def findFreeTrackLibPath():
    try:
        regKey = winreg.OpenKey(winreg.HKEY_CURRENT_USER, "Software\Freetrack\FreetrackClient")
        keyValue = winreg.QueryValueEx(regKey, "Path")
        winreg.CloseKey(regKey)
        if keyValue:
            return keyValue[0] + FREE_TRACK_LIB_FILE_NAME
        else:
            raise Exception()
    except:
        raise Exception("FreeTrack client DLL not found.")


class TFreeTrackData(ctypes.Structure):
    _fields_= [
        ("dataID", ctypes.c_ulong),
        ("camWidth", ctypes.c_long),
        ("camHeight", ctypes.c_long),
        ("yaw", ctypes.c_float),
        ("pitch", ctypes.c_float),
        ("roll", ctypes.c_float),
        ("x", ctypes.c_float),
        ("y", ctypes.c_float),
        ("z", ctypes.c_float),
        ("rawyaw", ctypes.c_float),
        ("rawpitch", ctypes.c_float),
        ("rawroll", ctypes.c_float),
        ("rawx", ctypes.c_float),
        ("rawy", ctypes.c_float),
        ("rawz", ctypes.c_float),
        ("x1", ctypes.c_float),
        ("y1", ctypes.c_float),
        ("z2", ctypes.c_float),
        ("y2", ctypes.c_float),
        ("x3", ctypes.c_float),
        ("y3", ctypes.c_float),
        ("x4", ctypes.c_float),
        ("y4", ctypes.c_float)
    ]


class FreeTrackMonitor:
    def __init__(self, settings):
        try:
            self.lib = ctypes.WinDLL(settings["freeTrackLibPath"])
            self.lib.FTGetData.restype = ctypes.c_bool
            self.lib.FTGetData.argtype = [TFreeTrackData]
        except:
            pass
    
    def getData(self):
        data = ctypes.pointer(TFreeTrackData())
        if self.lib.FTGetData(data):
            return data.contents
        else:
            raise Exception()



def main(argv):
    global ftm;
    p = pyaudio.PyAudio()
    
    # Set defaults
    nreps = NREPS;
    startTime = START_TIME;
    bufferSz = BUFFER;
    deviceid = p.get_default_output_device_info()['index'];
    triggerLen = TRIGGER_LEN;
    triggerChan = TRIGGER_CHAN;
    name = NAME;
    source_id = SOURCE_ID;
    ctrlName = None;
    ctrl_id = None;
    ctrlTrig = CTRL_TRIG;
    mixAudio = MIX_AUDIO;
    mixLSL = MIX_LSL;
    faceTrack = FACETRACK;
    
    # Open WAV files
    wf = [];
    for i in range(1,len(argv)):
        if not '.wav' in argv[i]:
            break;
        wf.append(wave.open(sys.argv[1],'rb'));
    if len(wf) == 0 and len(argv) == 1:
        print 'Error: invalid option(s). Show help using: ' + argv[0] + ' -h';
        sys.exit(2);
    
    # Parse options
    try:
        opts, args = getopt.getopt(argv[len(wf)+1:], 'h', 
                                   ['nreps=', 'starttime=', 'buffer=', 'triggerlen=', \
                                   'triggerchan=', 'deviceid=', 'name=', 'id=', 'ctrlname=', \
                                   'ctrlid=', 'mixaudio=', 'mixlsl=', 'facetrack=']);
    except getopt.GetoptError:
        print 'Error: invalid option(s). Show help using: ' + argv[0] + ' -h';
        sys.exit(2);

    for opt, arg in opts:
        if opt == '-h':
            
            print '\n' + argv[0] + ' wavfile1 wavfile2 ... wavfileN [options]';
            print '\t--nreps        [' + str(nreps) + '] number of times to play audio (-1 = loop indefinitely).';
            print '\t--starttime    [' + str(startTime) + '] number of seconds before wav file(s) start playing after first trigger.';
            print '\t--buffer       [' + str(bufferSz) + '] frame size of recording buffer.';
            print '\t--deviceid     [' + str(deviceid) +'] output device id.';
            print '\t--triggerlen   [' + str(triggerLen) + '] duration of trigger in samples.';
            print '\t--triggerchan  [' + str(triggerChan) + '] trigger channel index. A negative number will use all channels.';
            print '\t--name         [' + name + '] name of outgoing LabStreamingLayer stream.'
            print '\t--id           [' + source_id + '] source ID of LabStreamingLayer stream.';
            print '\t--ctrlname     name of control LabStreamingLayer stream (inactive by default).';
            print '\t--ctrlid       ID of control LabStreamingLayer stream.';
            print '\t--ctrltrig     [' + str(ctrlTrig) + '] delay in seconds after which to send trigger when control stream detected.'
            print '\t--mixaudio     [' + str(mixAudio) + '] mix sound card output.';
            print '\t--mixlsl       [' + str(mixLSL) + '] mix LSL output.';
            print '\t--facetrack    [' + str(faceTrack) + '] add FaceTrack channel to outgoing LabStreamingLayer stream.'
            print '\nList of Output Devices:';
            
            info = p.get_host_api_info_by_index(0);
            numdevices = info.get('deviceCount')
            for i in range(0, numdevices):
                if (p.get_device_info_by_host_api_device_index(0, i).get('maxOutputChannels')) > 0:
                    print "Output Device id ", i, " - ", \
                        p.get_device_info_by_host_api_device_index(0, i).get('name'), " - ", \
                        str(p.get_device_info_by_host_api_device_index(0, i).get('maxOutputChannels')) , \
                        ' channels';
            return;
        elif opt == '--nreps':
            nreps = int(arg);
        elif opt == '--starttime':
            startTime = float(arg);
        elif opt == '--buffer':
            bufferSz = int(arg);
        elif opt == '--deviceid':
            deviceid = int(arg);
        elif opt == '--triggerlen':
            triggerLen = int(arg);
            assert triggerLen <= bufferSz, 'Trigger length must be <= buffer length.';
        elif opt == '--triggerchan':
            triggerChan = int(arg);
        elif opt == '--name':
            name = arg;
        elif opt == '--id':
            source_id = arg;
        elif opt == '--ctrlname':
            ctrlName = arg;
        elif opt == '--ctrlid':
            ctrl_id = arg;
        elif opt == '--ctrltrig':
            ctrlTrig = arg;
        elif opt == '--mixaudio':
            if arg.lower() == 'true':
                mixAudio = True;
            else:
                mixAudio = False;
        elif opt == '--mixlsl':
            if arg.lower() == 'true':
                mixLSL = True;
            else:
                mixLSL = False;
        elif opt == '--facetrack':
            if arg.lower() == 'true':
                faceTrack = True;
            else:
                faceTrack = False;
    
    if nreps == -1:
        nreps = float('Inf');
    
    fsample = wf[0].getframerate();
    channels = wf[0].getnchannels();
    width = wf[0].getsampwidth();
    if width == 1:
        dtype = numpy.int8;
    elif width == 2:
        dtype = numpy.int16;
    elif width == 4:
        dtype = numpy.int32;
    elif width == 8:
        dtype = numpy.int64;
    else:
        print "Unsupported sample width.";
        sys.exit(2);
    
    # Check all wav files have the same specs as wf[0]
    for i in range(1,len(wf)):
        if wf[i].getframerate() != fsample:
            print argv[i+1] + ' does not have the same sample rate as ' + argv[1];
            sys.exit(2);
        elif wf[i].getnchannels != channels:
            print argv[i+1] + ' does not have the same number of channels as ' + argv[1];
        elif wf[i].getsampwidth() != width:
            print argv[i+1] + ' does not have the same sample width as ' + argv[1];
    
    # Print runtime configuration
    print '[*] WAV sampling rate: %f' % fsample;
    print '[*] Number of WAV channels: %d' % channels;
    print '[*] Frame size of recording buffer: %d' % bufferSz;
    print '[*] LSL stream name: %s' % name;
    print '[*] LSL stream ID: %s' % source_id;
    
    # Default mixing matrix
    mixingMat = numpy.eye(channels,dtype=NP_FORMAT);
    
    extChannels = max(0,triggerChan+1-channels);  # Additional channels to provide trigger
    if faceTrack:                                 # Channel for FaceTrack data
        ftm = FreeTrackMonitor({"freeTrackLibPath":findFreeTrackLibPath()});
        faceTrackChannels = 1;
    else:
        ftm = None;
        faceTrackChannels = 0;
    
    # Start LSL stream
    info = StreamInfo(name,'Audio',channels+extChannels+faceTrackChannels,float(fsample),LSL_FORMAT,source_id);
    outlet = StreamOutlet(info);
    
    # Streaming callback function and associated variables
    count = [0];    # Number of times stream has played
    wfIx = [0];     # Iterate through wf
    samples = [0.]; # Number of samples sent
    def callback(in_data, frame_count, time_info, status):
        global sendTrigger;
        global dirtyTrigger;
        global pauseFlag;
        global zeros;
            
        if samples[0] < startTime*fsample or pauseFlag:                          # Play zeros
            dataLSL = numpy.zeros([frame_count,channels+extChannels+faceTrackChannels],dtype=NP_FORMAT);
            dataMix = dataLSL;
            frames = frame_count;
        else:                                                                   # Play wav
            data = wf[wfIx[0]].readframes(frame_count);
            if len(data) < frame_count*channels*width and count[0] < nreps:     # End of wav file
                wf[wfIx[0]].rewind();
                wfIx[0] = (wfIx[0]+1) % len(wf);                                # Advance to next file
                if wfIx[0] == 0:
                    count[0] += 1;
                data = data + wf[wfIx[0]].readframes(frame_count - len(data)/channels/width);
            dataLSL = numpy.fromstring(data, dtype=dtype);
            dataLSL = numpy.divide(NP_FORMAT(dataLSL),2**(width*8-1));          # Convert to floating point
            
            frames = dataLSL.shape[0]/channels;
            dataLSL = dataLSL.reshape(frames,channels);
            dataMix = numpy.dot(dataLSL,mixingMat);
            
            if zeros is None:
                zeros = numpy.zeros((frames,extChannels+faceTrackChannels));
            dataLSL = numpy.concatenate((dataLSL,zeros),axis=1);
            dataMix = numpy.concatenate((dataMix,zeros),axis=1);

            if ftm:
                yaw = ftm.getData().yaw/numpy.pi;
                dataLSL[:,-1] += yaw;
                dataMix[:,-1] += yaw;
            
            #dataMix = dataMix.astype(NP_FORMAT);
            
        if sendTrigger == 1:
            print "Sending trigger!\n";
            if triggerChan >= 0:
                dataLSL[:triggerLen,triggerChan] = 1.;
                dataMix[:triggerLen,triggerChan] = 1.;
            else:
                dataLSL[:triggerLen,:] = 1.;
                dataMix[:triggerLen,:] = 1.;
            sendTrigger = 0;
        
        if mixLSL:
            outlet.push_chunk(x=dataMix.tolist(),timestamp=samples[0]/fsample);
        else:
            outlet.push_chunk(x=dataLSL.tolist(),timestamp=samples[0]/fsample);
            
        if dirtyTrigger:
            samples[0] += dataMix.shape[0];
        
        if mixAudio:
            return (dataMix[:,:channels+extChannels].astype(NP_FORMAT), pyaudio.paContinue);
        else:
            return (dataLSL[:,:channels+extChannels].astype(NP_FORMAT), pyaudio.paContinue);
    
    thread.start_new_thread(waitForKeyPress, ());   # Listen for trigger button press
    
    # Start streaming the WAV file
    p = pyaudio.PyAudio();
    stream = p.open(format=PA_FORMAT,channels=channels+extChannels,rate=fsample,input=False,
                    output_device_index=deviceid,frames_per_buffer=bufferSz,output=True,
                    stream_callback=callback);
    
    stream.start_stream();
    
    inInfo = None;  # Stream information
    inlet = None;   # StreamInlet
    try:
        print 'Streaming data (Press Ctrl-C to stop)...';
        while stream.is_active():
            if sendTrigger == 2:
                raise KeyboardInterrupt();
            
            # Scan for input stream
            if ctrlName is not None and inlet is None:
                streams = resolve_byprop('name',ctrlName,timeout=TIMEOUT);
                if len(streams) > 0:
                    if ctrl_id is not None:     # Scan for input stream IDs
                        inInfo = [i for i in streams if i.source_id() == ctrl_id];
                        if len(inInfo) == 0:
                            time.sleep(TIMEOUT);
                            continue;
                        else:
                            inInfo = inInfo[0];
                    else:
                        inInfo = streams[0];
                        
                    assert inInfo.channel_count() == mixingMat.size or \
                            inInfo.channel_count() == mixingMat.shape[0], \
                            '{} or {} control channels expected'.format(\
                            mixingMat.size, mixingMat.shape[0]);
                    inlet = StreamInlet(inInfo, recover=False);
                    if ctrlTrig > 0: thread.start_new_thread(trigDelay, (ctrlTrig));
                    print '\t[*] Control stream detected.'
            
            # Check input stream for samples
            if inlet is not None:
                try:
                    chunk,_ = inlet.pull_chunk(timeout=TIMEOUT);
                except:
                    # Inlet is probably broken
                    print '\t[*] Stream transmission broke off (input stream error); re-connecting...'
                    chunk = [];
                    inlet = None;
                if len(chunk) >= 1:
                    mixingMat = numpy.array(chunk[-1]).astype(NP_FORMAT);
                    if mixingMat.size == channels:
                        mixingMat = numpy.diag(mixingMat);
                    else:
                        mixingMat = mixingMat.reshape([channels,channels]);
            else:
                time.sleep(TIMEOUT);
    except KeyboardInterrupt:
        print ('\nStopping');
        stream.stop_stream();
        stream.close();
        p.terminate();
    return;
    
def trigDelay(d):
    global sendTrigger;
    global dirtyTrigger;
    time.sleep(d);
    sendTrigger = 1;
    dirtyTrigger = True;

def waitForKeyPress():
    global sendTrigger;
    global dirtyTrigger;
    global pauseFlag;
    while True:
        try:
            ch = raw_input();   # getch()
            if ch == 't':
                sendTrigger = 1;
                dirtyTrigger = True;
            elif ch == 'p':
                if pauseFlag:
                    print "Unpausing..."
                else:
                    print "Pausing..."
                pauseFlag = not pauseFlag
            elif ch == '\x03':      # ctrl-C
                sendTrigger = 2;
                break;
        except (KeyboardInterrupt, EOFError):
            sendTrigger = 2;
            break;

#def getch ():
#    fd = sys.stdin.fileno();
#    old_settings = termios.tcgetattr(fd);
#    try:
#        tty.setraw(sys.stdin.fileno());
#        ch = sys.stdin.read(1);
#    finally:
#        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings);
#    return ch;

if __name__ == "__main__":
    main(sys.argv);