# -*- coding: utf-8 -*-
"""
START_DEMO is the startup script for the Windows portion of the COCOHA realtime demo. It launches all
streaming modules in sequence.

Created on Thu Jan 18 16:23:33 2018

@author: Daniel
"""

import getpass
import os
import pylsl
import shutil
import socket
import subprocess
import sys
import time

USERNAME = getpass.getuser();
LOCALHOST = '127.0.0.1';

# Where OV config is stored
OVCFG_PATH = 'C:\Users\\' + USERNAME +'\AppData\Roaming\openvibe-2.1.0';

# Path to OV Acquisition Server
ACQUISITION_PATH = 'C:\Program Files (x86)\openvibe-2.1.0\openvibe-acquisition-server.cmd';

# Commands to start modules
SMARTING_PATH = 'C:\Users\\' + USERNAME + '\AppData\Local\Smarting Streamer\Smarting Streamer.exe';
WAV_PATH = ('python', 'wav2lsl.py', 'C:\Users\\' + USERNAME + '\Music\\frankenstein-oldman-stereo.wav', '--buffer', '2048', '--triggerlen', '64','--ctrlname','cca')
PREPROC_PATH = ('C:\Program Files (x86)\openvibe-2.1.0\openvibe-designer.cmd','--open', 'openvibe\\acquisition\\preproc_pilot_script_smarting.mxs');

# LSL streams expected from modules - configured acquisition server will be started when stream detected
# Set to None if not streaming via LSL (i.e. driver available in OV Acquisition Server)
SMARTING_LSL = 'EEG';
WAV_LSL = 'wav2lsl';
PREPROC_LSL = 'OVPreprocSignal';

# Parameters for starting acquisition server with configuration acq[1]
# Port acq[0] will be checked for stream.
SMARTING_ACQ = (1024,'openvibe\config\smartingacq.conf');
WAV_ACQ = (1025,'openvibe\config\wav2lslacq.conf');
PREPROC_ACQ = (1026,'openvibe\config\preprocacq.conf');


# Checks if port is open and streaming data
def checkPort(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
    result = s.connect_ex((ip, port));
    if not result:
        dat = s.recv(1024);
        if not dat: result = -1;
    s.close();
    return result;  # Returns 0 if port is streaming
    
def startStream(lslName, execPath, acq):
    # Start stream
    if lslName is not None:
        print("\tChecking for " + lslName + " stream...")
        stream = pylsl.resolve_byprop('name',lslName,timeout=0.);
        if not stream:
            # Exec
            subprocess.Popen(execPath);
            while not stream:
                stream = pylsl.resolve_byprop('name',lslName,timeout=0.);
                time.sleep(1);
        #stream = pylsl.resolve_byprop('name',lslName,timeout=0.);

        # Make sure there is data
        inlet = pylsl.StreamInlet(stream[0]);
        inlet.open_stream();
        while inlet.samples_available() == 0:
            time.sleep(1);
        inlet.close_stream();
    
    # Start Acquisition Server
    p = None;
    if acq is not None:
        if checkPort(LOCALHOST,acq[0]) != 0:           # Start acquisition server only if port is not streaming
            if OVCFG_PATH is not None:
                shutil.copy(acq[1],os.path.join(OVCFG_PATH,'openvibe-acquisition-server.conf'));
            p = subprocess.Popen((ACQUISITION_PATH,'--config',acq[1]));
            while checkPort(LOCALHOST,acq[0]) != 0:    # Wait until stream has started before continuing
                time.sleep(1);
                
    return p;

def main(argv):
    assert sys.platform == 'win32', 'Windows operating system expected.';
    
    p = list();
    try:
        p.append(startStream(SMARTING_LSL,SMARTING_PATH,SMARTING_ACQ));
        p.append(startStream(WAV_LSL,WAV_PATH,WAV_ACQ));
        p.append(startStream(PREPROC_LSL,PREPROC_PATH,PREPROC_ACQ));
    except KeyboardInterrupt:
        for it in p:
            if it is not None: it.kill();


if __name__ == '__main__':
    main(sys.argv);