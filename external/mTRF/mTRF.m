function [W,t,lambda] = mTRF(stim,resp,Fs,dir,start,fin,lambda,rtype,gpu,varargin)
% mTRF Multivariate temporal response function.
%   [W,T] = MTRF(STIM,RESP,FS) performs a ridge regression on the stimulus 
%   input STIM and the neural response data RESP, both with a sampling 
%   frequency FS, to solve for the multivariate temporal response function 
%   (mTRF) W and its time axis T. The mTRF represents the linear forward- 
%   mapping from RESP to STIM and is described by the equation:
% 
%                       w = inv(X*X'+lamda*M)*(X*Y')                         
%
%   where X is a matrix of stimulus lags, Y is the neural response, M is 
%   the regularisation term used to prevent overfitting and LAMBDA is the 
%   ridge parameter.
% 
%   [W,T] = MTRF(STIM,RESP,FS,DIR,START,FIN,LAMBDA) calculates the mTRF in 
%   the direction DIR. Pass in DIR==0 to use the default forward mapping or 
%   1 to use backward mapping. The time window over which the mTRF is 
%   calculated with respect to the stimulus is set between time lags START 
%   and FIN in milliseconds. The regularisation uses a ridge parameter 
%   LAMBDA. 
% 
%   Inputs:
%   stim     - stimulus input signal (frequency x time)
%   resp     - neural response data (channels x time)
%   Fs       - sampling frequency of stimulus and neural data in Hertz
%   dir      - direction of mapping: 0 = forward, 1 = backward (default = 0)
%   start    - start time lag of mTRF in milliseconds (default = -100ms)
%   fin      - stop time lag of mTRF in milliseconds (default = 400ms)
%   lambda   - ridge parameter for regularisation (default = max(XXT) for ridge1, ridge2 and 
%              shrinkage; default = 0.99 for normrc). For pinv and boosting, this parameter does not
%              apply and can be left empty.
%   rtype    - 'ridge1' ridge regression using diagonal
%            - ['ridge2'] ridge regression using Laplacian
%            - 'normrc' normalized reverse correlation (Nima's code)
%            - 'shrinkage'
%            - 'pinv' Moore-Penrose pseudoinverse
%            - 'boosting'
%   gpu      - use GPU if available: 0 = no, 1 = yes (default = 0)
%   varargin - argument value pairs for parameters used in lasso or boosting.
%              Lasso:
%                'alpha'
%              Boosting:
%                'W0'           initial weight matrix (default is a matrix of zeros).
%                'stepsize0'    the initial step size to use (default = 0.005).
%                'minstepsize'  the minimum step size to use (default = 0.005).
%                'max_fail'     the maximum number of iterations over which the validation score
%                               does not improve, resulting in the termination of the routine.
%                               (default = 2)
%                'min_it'       the minimum number of iterations.
%                'max_it'       maximum number of iterations divided by number of elements in W.
%                'val_len'      fraction of time samples used for validation.
%   
%   Outputs:
%   W      - mTRF (channels x frequency x time)
%   t      - time axis of mTRF in milliseconds (1 x time)
% 
%   See Readme for examples of use. 
%
%   See also mTRFpredict mTRFreconstruct.

%   References:
%      [1] Lalor EC, Pearlmutter BA, Reilly RB, McDarby G, Foxe JJ (2006). 
%          The VESPA: a method for the rapid estimation of a visual evoked 
%          potential. NeuroImage, 32:1549-1561.
%      [2] Lalor EC, Power AP, Reilly RB, Foxe JJ (2009). Resolving precise 
%          temporal processing properties of the auditory system using 
%          continuous stimuli. Journal of Neurophysiology, 102(1):349-359.

%   Author(s): Edmund Lalor & Lab, Trinity College Dublin
%              Telluride 2015 HAC Group: Jens Hjortkjærmm, Daniel D.E. Wong, Maarten de Vos, 
%                  Francisco Cervantes Constantino, Peter Diehl, Sahar Akram, Emina Alikovic, 
%                  Emily Graber, Carina Graverson, Elizabeth Margulis, Alain de Cheveigné, 
%                  Nima Mesgarani, Lucas Parra, Shihab Shamma, Malcolm Slaney
%   Email: edmundlalor@gmail.com
%   Website: http://sourceforge.net/projects/aespa/
%            http://neuromorphs.net/nm/wiki/2015/hac15
%   Version: 1.0
%   Last revision: 09 May 2014

persistent pXXT pXinfo;

if ~exist('start','var') || isempty(start)
    start = floor(-0.1*Fs);
else
    start = floor(start/1e3*Fs);
end
if ~exist('fin','var') || isempty(fin)
    fin = ceil(0.4*Fs);
else
    fin = ceil(fin/1e3*Fs);
end
window = length(start:fin);

if ~exist('dir','var') || isempty(dir) || dir == 0
    x = stim;
    y = resp;
    dir = 0;
elseif dir == 1
    x = resp;
    y = stim;
    [start,fin] = deal(-fin,-start);
end

if ~exist('gpu','var')  || isempty(gpu); gpu = 0; end;
if ~(gpu && exist('gpuDeviceCount','file') && gpuDeviceCount() > 0); gpu = 0; end;  % No GPU devices found

% Calculate XXT matrix
X = LagGenerator(x',start:fin)';
constant = ones(size(x,1),size(X,2));
X = [constant;X];
if isempty(pXXT) || ~all(pXinfo.size==size(X)) || pXinfo.begin ~= X(1) || pXinfo.last ~= X(end)
    XXT = X*X';
    pXXT = XXT;
    pXinfo.size = size(X);
    pXinfo.begin = X(1);
    pXinfo.last = X(end);
else
    XXT = pXXT;
end

% Calculate XY matrix
XY = X*y';

if ~exist('rtype','var') || isempty(rtype); rtype = 'shrinkage'; end;

% Set up regularisation parameter (lambda)
if ~exist('lambda','var') || isempty(lambda)    % Taken from Ed
    switch rtype
        case 'shrinkage'
            lambda = 0.3;   % From experience - an analytical estimate would be nice though
        case {'normrc','pinv'}
            lambda = 0.99;
        case {'boosting','lasso'}
            lambda = [];
        otherwise
            lambda = max(max(abs(XXT(size(stim,1):end,size(stim,1):end))));
    end
end

% Execute selected algorithm
if strcmp(rtype,'ridge2')
    XXT = gpuConvert(XXT, gpu); XY = gpuConvert(XY,gpu);
    d = 2*eye(size(XXT)); d(1,1) = 1; d(size(XXT,1),size(XXT,2)) = 1;
    u = [zeros(size(XXT,1)-1,1),eye(size(XXT)-1);zeros(1,size(XXT,2))];    
    l = [zeros(1,size(XXT,2));eye(size(XXT)-1),zeros(size(XXT,1)-1,1)];
    M = d-u-l;
    lambdaM = lambda*M;

    W = gather((XXT+lambdaM)\XY);
elseif strcmp(rtype,'ridge1')
    XXT = gpuConvert(XXT, gpu); XY = gpuConvert(XY,gpu);
    lambdaM = lambda*eye(size(XXT));
    W = gather((XXT+lambdaM)\XY);
elseif strcmp(rtype,'shrinkage')
    XXT = gpuConvert(XXT, gpu); XY = gpuConvert(XY,gpu);
    M = mean(eig(XXT))*eye(length(XXT));
    lambdaM = lambda*M;
    W = gather(((1-lambda)*XXT+lambdaM)\XY);
elseif strcmp(rtype,'normrc')
    XXT = gpuConvert(XXT, gpu); XY = gpuConvert(XY,gpu);
    [u,s,v] = svd(XXT); s = diag(s);
    energy = cumsum(s./sum(s));
    limit = find(energy>lambda,1);	% Threshold the energy
    invS = 1./s;
    if ~isempty(limit) && ((limit+1) <= length(invS)); invS(limit+1:end) = 0; end;
    RRinv = (v*diag(invS)*u');
    W = gather(RRinv*XY);
elseif strcmp(rtype,'pinv')
    XXT = gpuConvert(XXT, gpu); XY = gpuConvert(XY,gpu);
    [~,s] = svd(XXT); s = diag(s);
    energy = cumsum(s./sum(s));
    limit = find(energy>lambda,1);	% Threshold the energy
    if ~isempty(limit); tol = s(limit); else tol = 0; end;
    W = gather(pinv(XXT,tol)*XY);
elseif strcmp(rtype,'lasso')
    % Defaults
    val_len = 0.5;
    alpha = 1;
    
    for ii = 1:2:length(varargin)
        switch varargin{ii}
            case 'val_len'
                val_len = varargin{ii+1};
            case 'alpha'
                alpha = varargin{ii+1};
        end
    end
    
    W = zeros(size(X,1),size(y,1));
    for ii = 1:size(y,1)
        if isempty(lambda)                  % Null model
            [B,fitinfo] = lasso(X',y(ii,:),'Alpha',alpha,'NumLambda',1);
            lambda = fitinfo.Lambda;
            W(:,ii) = B;
        elseif lambda < 0 && val_len ~= 0   % Let MATLAB do a default sweep of lambda paramters
            [B,fitinfo] = lasso(X',y(ii,:),'Alpha',alpha,'CV',ceil(1/val_len));
            W(:,ii) = B(:,fitinfo.IndexMinMSE);
        elseif lambda < 0
            error('Lambda less than 0 specified. A non-zero validation length also is needed to sweep a range of lambda values.')
        else
            [B,~] = lasso(X',y(ii,:),'Alpha',alpha,'Lambda',lambda);
            W(:,ii) = B;
        end
    end
elseif strcmp(rtype,'boosting') % Solve X'*W = y';
    % Defaults
    W0 = zeros(size(X,1),size(y,1));
    stepsize0 = 0.005;
    minstepsize = 0.005;
    max_fail = 2;
    min_it = 10;
    max_it = 1e3;
    val_len = 0.5;
    
    % Obtain argument value pairs for boosting options
    for ii = 1:2:length(varargin)
        switch varargin{ii}
            case 'W0'
                W0 = varargin{ii+1};
                W0 = [zeros(size(x,1),size(y,1)); 
                      reshape(W0,numel(W0)/size(y,1),size(y,1))];
            case 'stepsize0'
                stepsize0 = varargin{ii+1};
            case 'minstepsize'
                minstepsize = varargin{ii+1};
            case 'max_fail'
                max_fail = varargin{ii+1};
            case 'min_it'
                min_it = varargin{ii+1};
            case 'max_it'
                max_it = varargin{ii+1};
            case 'val_len'
                val_len = varargin{ii+1};
        end
    end
    W = boost(X',y',W0,stepsize0,minstepsize,max_fail,min_it,max_it,val_len);
else
    error('Specified regularization method not supported');
end

% Calculate mTRF and its time axis
if dir == 0
    W = squeeze(reshape(W(size(stim,1)+1:end,:)',size(resp,1),size(stim,1),window));
elseif dir == 1
    W = squeeze(reshape(W(size(resp,1)+1:end,:)',size(stim,1),size(resp,1),window));
end
t = (start:fin)/Fs*1e3;


function W = boost(X,Y,W0,stepsize0,minstepsize,max_fail,min_it,max_it,val_len)
% Estimate W via boosting: maximize correlation coefficient between Y and Yhat, where Yhat = X*W

% Split data into training and validation sets
val_len = round(size(X,1)*val_len);
valX = X(end-val_len+1:end,:);
valY = Y(end-val_len+1:end,:);
trainX = X(1:end-val_len,:);
trainY = Y(1:end-val_len,:);

err_train = zeros(1,max_it+1);
err_val = zeros(1,max_it+1);

deltaW = stepsize0;

W = W0;
bestW = W;                  % best W based on validation
bestIt = 1;                 % best validation iteration
nfail = 0;                  % number of validation checks since best W
for ii = 1:max_it
    ypred_train = trainX*W;
    ypred_val = valX*W;
    
    if ii == 1
        % err_train(ii) = sum((ypred_train(:)-trainY(:)).^2);
        % err_val(ii) = sum((ypred_val(:)-valY(:)).^2);
        R = corrcoef(ypred_train(:),trainY(:)); err_train(ii) = 1-R(1,2);
        R = corrcoef(ypred_val(:),valY(:)); err_val(ii) = 1-R(1,2);
    end
    
    % Determine best element in W to change
    bestPos = 0; incsign = 0; minE = Inf;
    for jj = 1:numel(W)
        % A change in an element of W only affects the corresponding column in Y
        [sub(1), sub(2)] = ind2sub(size(W),jj);
        ypred = ypred_train; ypred(:,sub(2)) = ypred_train(:,sub(2)) + deltaW*trainX(:,sub(1));
        % e1 = sum((ypred(:)-trainY(:)).^2);  % Error from positive deltaW
        R = corrcoef(ypred(:),trainY(:)); e1 = 1-R(1,2);
        
        ypred = ypred_train; ypred(:,sub(2)) = ypred_train(:,sub(2)) - deltaW*trainX(:,sub(1));
        % e2 = sum((ypred(:)-trainY(:)).^2);  % Error from negative deltaW
        R = corrcoef(ypred(:),trainY(:)); e2 = 1-R(1,2);
        
        if e1<e2; incsign_tmp = 1; else incsign_tmp = -1; end;
        minE_tmp = min([e1 e2 minE]);
        if minE_tmp < minE
            bestPos = jj;
            incsign = incsign_tmp;
            minE = minE_tmp;
        end
    end
    
    % Update W
    W(bestPos) = W(bestPos) + incsign*deltaW;
%     ypred = trainX*W;
%     err_train(ii+1) = sum((ypred(:)-valX(:)).^2);
    err_train(ii+1) = minE;
    
    % Check validation and update bestW
    ypred = valX*W;
    % err_val(ii+1) = sum((ypred(:)-valY(:)).^2);
    R = corrcoef(ypred(:),valY(:)); err_val(ii+1) = 1-R(1,2);
    if err_val(ii+1) < err_val(bestIt) || isnan(err_val(bestIt))
        bestIt = ii+1;
        bestW = W;
    end
    
    % Check termination conditions
    if err_val(ii+1) > err_val(ii)
        nfail = nfail+1;
    else
        nfail = 0;
    end
    if nfail >= max_fail && ii > min_it; break; end;
    
    if minE < err_train(ii)     % Could not improve training error - reduce step size
        if deltaW/2 > minstepsize
            deltaW = deltaW/2;
        end
    end
    
    if mod(ii,numel(W))==0; fprintf('Iteration %i, best validation error: %d',ceil(ii/numel(W)),err_val(bestIt)); end;
end
fprintf('Boosting terminated on iteration %i\n',ceil(ii/numel(W)));
W = bestW;



function X = gpuConvert(X,gpu)
% Helper function for getting variables onto the GPU

if gpu && exist('gpuDeviceCount','file') && gpuDeviceCount() > 0
    X=gpuArray(X);
end