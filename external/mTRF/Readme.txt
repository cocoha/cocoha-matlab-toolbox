* The code contained in this repository was modified for the COCOA-MATLAB-toolbox based on work by the 2015 HAC team at the Telluride Neuromorphic Cognition Engineering Workshop to include a broad set of regularization options and implement some computational optimizations.

This repository contains code, example data and example Presentation files for implementing the so-called VESPA and AESPA methods for EEG (or MEG or ECoG).
Code:
mTRF – MTRF Multivariate temporal response function.

[W,T] = MTRF(STIM,RESP,FS) performs a ridge regression on the stimulus  input STIM and the neural response data RESP, both with a sampling frequency FS, to solve for the multivariate temporal response function  (mTRF) W and its time axis T. The mTRF is described by the equation:
                       w = inv(x*x'+lamda*M)*(x*y')                         
 where X is a matrix of stimulus lags, Y is the neural response, M is  the regularisation term used to prevent overfitting and LAMBDA is the  ridge parameter.

 [W,T] = MTRF(STIM,RESP,FS,LAMBDA,START,FIN) calculates the mTRF for a specified ridge parameter LAMBDA. The time window over which the mTRF is calculated with respect to the stimulus is set between time lags START and FIN in milliseconds.

 Inputs:
 stim   - stimulus input signal (frequency x time)
 resp   - neural response data (channels x time)
 Fs     - sampling frequency of stimulus and neural data in Hertz
 lambda - ridge parameter for regularisation (default = max(xxt))
 start  - start time lag of TRF in milliseconds (default = -120ms)
 fin    - stop time lag of TRF in milliseconds (default = 420ms)
   
 Outputs:
 w      - mTRF (channels x time x frequency)
 t      - time axis of mTRF in milliseconds (1 x time)


%   Examples: 
128-channel EEG was recorded at 512Hz. Stimulus was natural speech,  presented at 48kHz for 2 minutes. The envelope of the speech waveform  was obtained using a Hilbert transform and was then downsampled to 512Hz.
 %%%  Forward model  %%%
 load('speech_data.mat')
[W,t] = mTRF(speechEnvelope,EEG,512,0,-200,500,[]);
 figure; plot(t,w(85,:));

 %%%  Backward model  %%%

speech64 = resample(speechEnvelope, 64, 512);
EEG64 = resample(EEG', 64, 512)';
[G,t] = mTRF(speech64,EEG64,64,1,-200,500,[]);
figure; plot(t,G(85,:)); 

 %%%  Forward spectrotemporal model  %%%
load('spectroSpeech.mat')
 [W,t] = mTRF(spectro,EEG,512,0,-20,300,10);
figure;
surface(t,1:16,squeeze(W(85,:,:))); shading('flat'); caxis([-5.0 5.0])

 %%%  Backward spectrotemporal model  %%%
spectro64 = resample(spectro', 64, 512)';
[G,t] = mTRF(spectro64,EEG64,64,1,-20,300,[]);
figure; surface(t,1:128,squeeze(G(3,:,:))); shading('flat');

mTRFpredict – Multivariate Temporal Response Function Prediction Code
[RESP] = MTRFPREDICT(STIM,W,FS,START,FIN) given an input stimulus STIM, this function computes a prediction of the neural response based on the provided model W, previously evaluated with the function mTRF. The sampling frequency FS and the time-lags defined by START and FIN must be the same used for building W with the function mTRF. 
Inputs:
stim   - stimulus input signal (frequency x time)
W      - forward mTRF (channels x frequency x time)
Fs     - sampling frequency of stimulus and neural data in Hertz
start  - start time lag of mTRF in milliseconds (default = -100ms)
fin    - stop time lag of mTRF in milliseconds (default = 400ms)

Outputs:
resp   - neural response data (channels x time)

% Examples: 

128-channel EEG was recorded at 512Hz. Stimulus was natural speech, presented at 48kHz for 2 minutes. The envelope of the speech waveform  was obtained using a Hilbert transform and was then downsampled to 512Hz.

 %%%  Forward model  %%%
[W,t] = mTRF(speechEnvelope,EEG,512,0,-200,500,[]);
chan = 85;
figure; plot(t,W(chan,:));
[resp] = mTRFpredict(speechEnvelope,W,512,-200,500);
respToPlot = (resp(chan,:) - mean(resp(chan,:))) / std(resp(chan,:));
eegToPlot = (EEG(chan,:) - mean(EEG(chan,:))) / std(EEG(chan,:));
figure; hold on; plot(respToPlot, 'b'); plot(eegToPlot, 'r')
[r,p] = corr(respToPlot',eegToPlot');
disp(['[r, p] = [' num2str(r) ', ' num2str(p) ']' ])

 %%%  Forward spectrotemporal model  %%%
load('spectroSpeech.mat')
chan = 85;
 [W,t] = mTRF(spectro,EEG,512,0,-20,300,10);
figure;
surface(t,1:16,squeeze(W(chan,:,:))); shading('flat'); caxis([-5.0 5.0])
 [resp] = mTRFpredict(spectro,W,512,-20,300);
respToPlot = (resp(chan,:) - mean(resp(chan,:))) / std(resp(chan,:));
eegToPlot = (EEG(chan,:) - mean(EEG(chan,:))) / std(EEG(chan,:));
figure; hold on; plot(respToPlot, 'b'); plot(eegToPlot, 'r')
[r,p] = corr(respToPlot',eegToPlot');
disp(['[r, p] = [' num2str(r) ', ' num2str(p) ']' ])

mTRFpredict – Multivariate temporal response function prediction code.

[RESP] = MTRFRECONSTRUCT(RESP,G,FS,START,FIN) given a neural response EEG, this function computes a reconstruction of the input stimulus based on the provided model G, previously evaluated with the function mTRF. The sampling frequency FS and the time-lags defined by START and FIN must be the same used for building G with the function mTRF.

Inputs:
resp   - neural response data (channels x time)
G      - backward mTRF (channels x frequency x time)
Fs     - sampling frequency of stimulus and neural data in Hertz
start  - start time lag of mTRF in milliseconds (default = -100ms)
fin    - stop time lag of mTRF in milliseconds (default = 400ms)
  
Outputs:
stim   - stimulus input signal (frequency x time)

%   Examples:
128-channel EEG was recorded at 512Hz. Stimulus was natural speech, presented at 48kHz for 2 minutes. The envelope of the speech waveform  was obtained using a Hilbert transform and was then downsampled to 512Hz.

%%%  Backward model  %%%

speech64 = resample(speechEnvelope, 64, 512);
EEG64 = resample(EEG', 64, 512)';
[G,t] = mTRF(speech64,EEG64,64,1,-200,500,[]);
[stim] = mTRFreconstruct(EEG64,G,64,-200,500);
recToPlot = (stim - mean(stim)) / std(stim);
stimToPlot = (speech64 - mean(speech64)) / std(speech64);
figure; hold on; plot(recToPlot, 'b'); plot(stimToPlot, 'r')
[r,p] = corr(recToPlot',stimToPlot');
disp(['[r, p] = [' num2str(r) ', ' num2str(p) ']' ])

%%%  Backward spectrotemporal model  %%%

spectro64 = resample(spectro', 64, 512)';
[G,t] = mTRF(spectro64,EEG64,64,1,-20,300,[]);
figure;
surface(t,1:128,squeeze(G(3,:,:))); shading('flat');
[stim] = mTRFreconstruct(EEG64,G,64,-20,300);
freqIdx = 1;
stimToPlot = (stim(freqIdx,:) - mean(stim(freqIdx,:))) / std(stim(freqIdx,:));
eegToPlot = (EEG64(freqIdx,:) - mean(EEG64(freqIdx,:))) / std(EEG64(freqIdx,:));
figure; hold on; plot(stimToPlot, 'b'); plot(eegToPlot, 'r')
[r,p] = corr(stimToPlot',eegToPlot');
disp(['[r, p] = [' num2str(r) ', ' num2str(p) ']' ])

%% Training/Testing on distinct chunks of data

%%%  Backward model  %%%

speech64 = resample(speechEnvelope, 64, 512);
EEG64 = resample(EEG', 64, 512)';
len = size(EEG64,2);
speechTrain = speech64(1:len/2);
speechTest  = speech64(len/2+1:end);
EEGTrain    = EEG64(:,1:len/2);
EEGTest     = EEG64(:,len/2+1:end);
[G,t] = mTRF(speechTrain,EEGTrain,64,1,-200,500,[]);
[stim] = mTRFreconstruct(EEGTest,G,64,-200,500);
recToPlot = (stim - mean(stim)) / std(stim);
stimToPlot = (speechTest - mean(speechTest)) / std(speechTest);
figure; hold on; plot(recToPlot, 'b'); plot(stimToPlot, 'r')
[r,p] = corr(recToPlot',stimToPlot');
disp(['[r, p] = [' num2str(r) ', ' num2str(p) ']' ])


Data:
contrast_data
This file contains 128 channels of EEG recorded at 512 Hz. It also contains a sequence of numbers that indicate the contrast of a checkerboard that was presented at a rate of 60 Hz while this EEG was acquired. This sequence is normalized to between 0 and 1. See Lalor et al., 2006 for details.

coherentMotion_data
This file contains 128 channels of EEG recorded at 512 Hz. It also contains a sequence of numbers that indicate how coherent the motion of a dot field was that was presented while this EEG was acquired. See Gonçalves et al., 2014 for details.

speech_data
This file contains 128 channels of EEG recorded at 512 Hz. It also contains a sequence of numbers that indicate the envelope of a speech stimulus that was presented while this EEG was acquired. See Lalor & Foxe, 2010 for details.

spectroSpeech
This file contains 128 channels of EEG recorded at 512 Hz. It also contains 16 sequences of numbers, each one for a distinct and non-overlapped frequency band from 250 to 8000 Hz, that indicate the envelope of the speech stimulus presented to the subject, after having filtered it in one of the 16 frequency bands. See Di Liberto et al, 2015 for details.


Presentation files:
contrastVESPA.zip – a folder containing Presentation (Neurobehavioral Systems) scripts to modulate the contrast of a checkerboard on every refresh of a computer monitor according to a stochastic signal. The folder also contains checkerboard bitmaps.
coherentMotionVESPA.zip – a folder containing Presentation (Neurobehavioral Systems) scripts to present a random dot field that whose coherent level is modulated by a stochastic signal. The folder also contains checkerboard bitmaps.

Note: No stimulus presentation files are given for speech or audio stimuli as these simply involve presenting a wav file and ensuring that the presentation of that wav file is correctly aligned with the EEG. 

References:
Gonçalves NR, Whelan R, Foxe JJ, Lalor EC (2014). Towards obtaining spatiotemporally precise responses to continuous sensory stimuli in humans: a general linear modeling approach to EEG. NeuroImage, doi: 10.1016/j.neuroimage.2014.04.012.
Lalor, E. C., & Foxe, J. J. (2010). Neural responses to uninterrupted natural speech can be extracted with precise temporal resolution. European Journal of Neuroscience, 31(1):189-193.
Lalor, E. C.,  Pearlmutter, B. A., & Foxe, J. J. Reverse correlation and the VESPA method. In: Handy, T. C. (Ed.), Brain Signal Analysis: Advances in Neuroelectric and Neuromagnetic Methods. Cambridge, MA: MIT Press, pp 1-19.
Lalor, E. C., Pearlmutter, B. A., Reilly, R. B., McDarby, G., & Foxe, J. J. (2006). The VESPA: a method for the rapid estimation of a visual evoked potential. NeuroImage, 32, 1549-1561.
